package sqs.pematangsiantarkota.com;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import android.content.Context;
import android.location.Location;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SebastianManager extends ReactContextBaseJavaModule {
	
	protected Context context = null;
	
	public SebastianManager(ReactApplicationContext reactContext) {
		super(reactContext);
		this.context = reactContext.getApplicationContext();
	}
	
	@Override
	public String getName() {
		return "SebastianManager";
	}
	
	@ReactMethod
	public void GetLocation(Callback callback) {
		String greeting = "";

		JSONObject jObjectFinally = new JSONObject();
		
		GPStracker g = new GPStracker(context);
        Location l = g.getLocation();
        if(l != null) {
            double lat = l.getLatitude();
            double lon = l.getLongitude();
			if(String.valueOf(lat).equals("") || String.valueOf(lat).equals("undefined") || String.valueOf(lon).equals("") || String.valueOf(lon).equals("undefined"))
			{	try {
					jObjectFinally.put("latitude", "2.958493");
					jObjectFinally.put("longitude", "99.064523");
				} catch (JSONException e) { }
			}
			else
			{	try {
					jObjectFinally.put("latitude", String.valueOf(lat));
					jObjectFinally.put("longitude", String.valueOf(lon));
				} catch (JSONException e) { }
			}
		}
		greeting = jObjectFinally.toString();
		callback.invoke(greeting);
	}
	
}