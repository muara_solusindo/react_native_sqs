import { StyleSheet, Dimensions } from 'react-native';
import { RecipeCard } from '../../AppStyles';
const{width : WIDTH} = Dimensions.get('window');
const styles = StyleSheet.create({
	container: RecipeCard.container,
	photo: RecipeCard.photo,
	photohome: RecipeCard.photohome,
	title: RecipeCard.title,
	category: RecipeCard.category,
	// logoPemerintah : {
	// 	width : 110,
	// 	height : 137 ,
	// 	marginTop : 60,
	// 	marginBottom : 40,
	// 	padding : 15,
	// },
	kotakLuar: {
		// backgroundColor: 'white',
		// backgroundColor: 'white',
		flex: 1,
		    // flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	},
	kotakBiru : {
		backgroundColor: '#1ba0e1',
		height : 120,
	},
	luarAbu :{
		backgroundColor : '#f1f1f1', 
	},
	profilLuar :{
		height : 220
	},
	blue :{
		height : 250, 
		backgroundColor : '#1ba0e1',
		zIndex: 0
	},
	contents:{
		marginTop: -250,
		zIndex: 6
	},


	profilLuarBiru : {
		backgroundColor : '#1ba0e1', 
		height : 250, 
	},

	profilCard : {
		flexDirection : 'row' , 
		backgroundColor : '#ffffff', 
		height : 520, 
		marginHorizontal : 20, 
		borderRadius : 7,
		shadowColor: "#000000",
		shadowOpacity: 0.9,
		shadowRadius: 6,
		elevation : 5,
		justifyContent : 'space-between',
		marginBottom: 10
		// width : '100%'
	},

	profilLuarImage  : {
		padding : 20,
		margin : 20 ,
		alignItems: 'center',
	},
	profilImage : {
		height : 97, 
		width : 97,
	},

	shadowprofilImage : {
		alignItems: 'center',
		justifyContent:'center',
		backgroundColor: 'white', 
		width: 96, 
		height: 96, 
		borderRadius: 50,
		shadowColor: '#000',
		shadowOpacity: 1,
		elevation : 10,
	},

	profilAkun : {
		color : 'white',
		paddingTop : 12,
	},

	profilNama : {
		fontWeight : 'bold'
	},

	profilEmail : {
		fontSize : 13, 
		fontStyle: 'italic'
	},

	profilNoHp : {
		fontSize : 13
	},
	mainProfil : {
		width : `${100/3}%`
	},
	mainProfilTengah : {
		// borderRightWidth : 2, 
		// borderLeftWidth : 2, 
		// borderColor : '#e8f1f4',
		shadowColor: "#000000",
		shadowOpacity: 0.18,
		shadowRadius: 1,
		elevation : 2,
		marginTop : -1,
		marginBottom : -1,
		// margin : 5,
		// borderRadius : 5,
		shadowOffset: {
			width: 0,
			height: 1,
		},

	},
	luarMainProfil : {
		paddingTop : 15, 
		alignItems : 'center', 
		justifyContent : 'center'
	},
	shadowImageProfil : {
		backgroundColor: 'white', 
		width: 40, 
		height: 40, 
		borderRadius: 40,
		shadowColor: '#000',
		shadowOpacity: 1,
		elevation : 6,
	},
	namaMainProfil : {
		fontSize : 12, 
		paddingTop : 10
	},
	luarInfromasiProfile : {
		marginTop : 300
	},
	boxInfromasiProfile : {
		flexDirection: 'row', 
		paddingLeft : 80,
		paddingBottom : 20,
	},
	infromasiProfilImage : {
		height : 30 ,
		width : 30 ,
		marginRight: 25,
	},
	informasiProfilNama : {
		fontSize : 15, 
		marginTop : 5,
	}, 
	italic : {
		fontStyle : 'italic'
	},
	luarBtnLogout : {
		marginTop : 30,
		alignItems : 'center'
	},
	btnLogout : {
		height : 38,
		width : 130,
		backgroundColor: '#1ba0e1', 
		borderRadius : 10,
		alignItems : 'center',
		justifyContent : 'center',
		flexDirection : 'row'

	},
	btnName : {
		color : 'white',
		fontWeight : 'bold',
		paddingLeft : 8
	},
	shadowBtnLogout : {
		backgroundColor: 'white', 
		height : 38,
		width : 130, 
		borderRadius: 40,
		shadowColor: '#000',
		shadowOpacity: 1,
		elevation : 6,
	},
	btnEye : {
		position : 'absolute',
		top : 12,
		right : 37 
	},
	inputContainer : {
		marginTop : 10, 
	},
	input : {
			width :  WIDTH - 90,
			height : 45,
			borderRadius: 25,
			fontSize : 16,
			paddingLeft : 45,
			backgroundColor : 'rgba(255,255,255,0.45)',
			color : 'black',
			marginHorizontal: 25,
			marginVertical: 5 ,
			borderWidth : 1.5,
			borderColor : 'rgba(27,160,255,0.5)'
	},
	inputIcon : {
		position : 'absolute',
		top : 12,
		left : 37 
	}
	

});

export default styles;
