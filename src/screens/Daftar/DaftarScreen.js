import React , {Component}from 'react';
import {
  Text,
  View,
  Dimensions,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ToastAndroid,
} from 'react-native';
import styles from './styles';
import BackButton from '../../components/BackButton/BackButton';
const{width : WIDTH} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';
import Spinner from 'react-native-loading-spinner-overlay';

import UrlServer from '../../UrlServer.js';
let URL_SQS = UrlServer.getUrl();
var SQLite = require('react-native-sqlite-storage');
const db = SQLite.openDatabase(UrlServer.getNamaDatabaseSQLite(), '1.0', '', 1);

export default class DaftarScreen extends React.Component {
	static navigationOptions = {
		header: null
	};
	
	constructor(){
		super()
		this.state ={
			showPass : true,
			press : false,
			nama_lengkap : '',
			handphone : '',
			email : '',
			username : '',
			password_login : '',
			ulangi_password : '',
			loading : false,
		}
	}

	showPass = () => {
		if(this.state.press == false){
			this.setState({ showPass : false, press : true})
		}else{
			this.setState({ showPass : true, press : false})
		}
	}
	onPressLupa = () => {
		this.props.navigation.navigate('LupaPassword');
	};
	
	daftar = () => {
		
		var str = this.state.email;
		var n = str.search("@") * 1;
		
		if(this.state.nama_lengkap === "" || this.state.handphone === "" || this.state.email === "" || this.state.username === "" || this.state.password_login === "" || this.state.ulangi_password === "")
		{	alert('Isi data diri anda!');
		}
		else if(n < 0)
		{	alert('E-Mail tidak valid!');
		}
		else
		{
			this.setState({ loading : true });
			
			fetch( URL_SQS + '/frontend/web/index.php?r=site/signupandroid', {
				method: 'POST',
				headers: 
				{
					'Accept': 'application/json',
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(
				{
					nama_lengkap : this.state.nama_lengkap,
					handphone : this.state.handphone,
					email : this.state.email,
					username : this.state.username,
					password_login : this.state.password_login,
					ulangi_password : this.state.ulangi_password,
				})
			})
			.then((response) => response.json())
			.then((response) => {
				if(response[0].status == "1")
				{	const { navigate } = this.props.navigation;
					navigate('Login', { URL_SQS : URL_SQS});
					ToastAndroid.show('Anda telah berhasil mendaftar, silahkan Login', ToastAndroid.SHORT);
				}
				else if(response[0].status == "2")
				{	alert('Password tidak sama!');
				}
				else if(response[0].status == "3")
				{	alert('Username sudah digunakan akun lain!');
				}
				else if(response[0].status == "4")
				{	alert('Password minimal 6 karakter!');
				}
				else
				{	alert('Penulisan email salah!');
				}
				this.setState({ loading : false });
			})
			.catch((error) => {
				this.setState({ loading: false });
			});
			
		}
	}
	
	render() {
		return (
			<View style={{flex : 1}}>
				
				<ScrollView>
					<Spinner visible={this.state.loading} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
					<View style={[styles.blue]}/>

					<View style={styles.contents}>
						<View style={styles.profilLuarImage}>
							<View style={styles.shadowprofilImage}>
								<Image style={styles.profilImage} source={require('../../../assets/icons/sqs-bulet.png')}/>
							</View>
							<Text style={styles.profilAkun}> Siantar Quick Service</Text>
						</View>
						<View style={styles.profilCard}>
							<View style={{justifyContent: 'center', alignItems: 'center', marginVertical : 20}}>
								<View>
									<Text style={{fontSize : 15, fontStyle : 'italic', marginBottom : 5}}>Daftar Akun</Text>
								</View>
								<View style={styles.inputContainer}>
								<Icon name={'ios-person-add'} size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
								<TextInput 
									style={styles.input}
									placeholder={'Nama Lengkap'}
									placeholderTextColor={'rgba(255,255,0.7)'}
									underlineColorAndroid = 'transparent'
									onChangeText={(nama_lengkap) => this.setState({nama_lengkap})}
								/>
							</View>
							<View style={styles.inputContainer}>
								<Icon name={'ios-call'} size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
								<TextInput 
									style={styles.input}
									placeholder={'No. Handphone'}
									placeholderTextColor={'rgba(255,255,0.7)'}
									underlineColorAndroid = 'transparent'
									onChangeText={(handphone) => this.setState({handphone})}
								/>
							</View>
							<View style={styles.inputContainer}>
								<Icon name={'ios-mail'} size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
								<TextInput 
									style={styles.input}
									placeholder={'Email'}
									placeholderTextColor={'rgba(255,255,0.7)'}
									underlineColorAndroid = 'transparent'
									onChangeText={(email) => this.setState({email})}
								/>
							</View>

							<View style={styles.inputContainer}>
								<Icon name={'ios-person'} size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
								<TextInput 
									style={styles.input}
									placeholder={'Username'}
									placeholderTextColor={'rgba(255,255,0.7)'}
									underlineColorAndroid = 'transparent'
									onChangeText={(username) => this.setState({username})}
								/>
							</View>
						
						
							<View style={styles.inputContainer}>
								<Icon name={'ios-lock'} size={28} colot={'rgba(255,255,255,0.7)'} style={styles.inputIcon}/>
								<TextInput 
									style={styles.input}
									placeholder={'Password'}
									placeholderTextColor={'rgba(255,255,0.7)'}
									underlineColorAndroid = 'transparent'
									secureTextEntry={this.state.showPass}
									onChangeText={(password_login) => this.setState({password_login})}
								/>
								<TouchableOpacity style={styles.btnEye} onPress={this.showPass.bind(this)}>
									<Icon name={this.state.showPass == false ? 'ios-eye' :  'ios-eye-off'} size={26} colot={'rgba(255,255,255,0.7)'} />
								</TouchableOpacity>
							</View>
							<View style={styles.inputContainer}>
								<Icon name={'ios-lock'} size={28} colot={'rgba(255,255,255,0.7)'} style={styles.inputIcon}/>
								<TextInput 
									style={styles.input}
									placeholder={'Ulangi Password'}
									placeholderTextColor={'rgba(255,255,0.7)'}
									underlineColorAndroid = 'transparent'
									secureTextEntry={this.state.showPass}
									onChangeText={(ulangi_password) => this.setState({ulangi_password})}
								/>
								<TouchableOpacity style={styles.btnEye} onPress={this.showPass.bind(this)}>
									<Icon name={this.state.showPass == false ? 'ios-eye' :  'ios-eye-off'} size={26} colot={'rgba(255,255,255,0.7)'} />
								</TouchableOpacity>
							</View>
				
								<View style={styles.luarBtnLogout}>
									<View style={styles.shadowBtnLogout}> 
										<TouchableOpacity style={styles.btnLogout} onPress = {this.daftar}> 
											<Text style={styles.btnName}>Daftar Sekarang</Text>
										</TouchableOpacity>
									</View>
								</View>
								
							</View>
						</View>
					</View>

				</ScrollView>
			</View>
		);
	}
}
