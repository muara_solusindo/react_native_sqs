import React , {Component}from 'react';
import {
  Text,
  View,
  Dimensions,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import BackButton from '../../components/BackButton/BackButton';
const{width : WIDTH} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';
export default class GantiProfilScreen extends React.Component {
	static navigationOptions = {
		header: null
	};
	
	constructor(){
		super()
		this.state ={
			showPass : true,
			press : false
		}
	}

	showPass = () => {
		if(this.state.press == false){
			this.setState({ showPass : false, press : true})
		}else{
			this.setState({ showPass : true, press : false})
		}
	}
	onPressLupa = () => {
		this.props.navigation.navigate('LupaPassword');
	};
	
  render() {
    return (
		<View style={{flex : 1}}>
			
			<ScrollView>

				<View style={[styles.blue]}/>

				<View style={styles.contents}>
					<View style={styles.profilLuarImage}>
						<View style={styles.shadowprofilImage}>
							<Image style={styles.profilImage} source={require('../../../assets/icons/sqs-bulet.png')}/>
						</View>
						<Text style={styles.profilAkun}> Siantar Quick Service</Text>
					</View>
					<View style={styles.profilCard}>
						<View style={{justifyContent: 'center', alignItems: 'center', marginVertical : 20}}>
							<View>
								<Text style={{fontSize : 15, fontStyle : 'italic', marginBottom : 5}}>Ganti Profil</Text>
							</View>

							<View style={styles.inputContainer}>
								<Icon name={'ios-person-add'} size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
								<TextInput 
									style={styles.input}
									placeholder={'Nama Lengkap'}
									placeholderTextColor={'rgba(255,255,0.7)'}
									underlineColorAndroid = 'transparent'
								/>
							</View>
							<View style={styles.inputContainer}>
								<Icon name={'ios-mail'} size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
								<TextInput 
									style={styles.input}
									placeholder={'Email'}
									placeholderTextColor={'rgba(255,255,0.7)'}
									underlineColorAndroid = 'transparent'
								/>
							</View>
							<View style={styles.inputContainer}>
								<Icon name={'ios-call'} size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
								<TextInput 
									style={styles.input}
									placeholder={'No. Handphone'}
									placeholderTextColor={'rgba(255,255,0.7)'}
									underlineColorAndroid = 'transparent'
								/>
							</View>

							<View style={styles.luarBtnLogout}>
								<View style={styles.shadowBtnLogout}> 
									<View style={styles.btnLogout}> 
										<Text style={styles.btnName}>Ubah Data</Text>
									</View>
								</View>
							</View>
							
						</View>
					</View>
				</View>

			</ScrollView>
		</View>
    );
  }
}
