import React , {Component}from 'react';
import {
	Text,
	View,
	Dimensions,
	ScrollView,
	Image,
	TouchableOpacity,
	TextInput,
	NativeModules,
	ToastAndroid,
} from 'react-native';
import styles from './styles';
import ImagePicker from 'react-native-image-picker';
import PickerModal from 'react-native-picker-modal-view';
import BackButton from '../../components/BackButton/BackButton';
const { width: viewportWidth } = Dimensions.get('window');
import Spinner from 'react-native-loading-spinner-overlay';

import UrlServer from '../../UrlServer.js';
let URL_SQS = UrlServer.getUrl();
var SQLite = require('react-native-sqlite-storage');
const db = SQLite.openDatabase(UrlServer.getNamaDatabaseSQLite(), '1.0', '', 1);

let Username = '';
let jenisPengaduan = null;

const optionsImage = {
	title : 'Select a Photos',
	takePhotoButtonTitle : 'Take a Photo',
	chooseFromLibraryButtonTitle : 'Choose from Gallery',
	maxWidth : 350,
	maxHeight : 350,
	quality : 1,
};
var photo = null;

const SebastianManager = NativeModules.SebastianManager;

export default class ProfilScreen extends React.Component {
	
	static navigationOptions = ({ navigation }) => {
		return {
			headerTransparent: 'true',
			headerLeft: (
				<BackButton
					onPress={() => {
						navigation.goBack();
					}}
				/>
			)
		};
	};
	
	constructor(props){
		super(props);
		this.state = {
			selectedItemSKPD : {},
			itemsSKPD : [],
			keterangan : '',
			latitude : '',
			longitude : '',
			imageSource : null,
			username : null,
			loading : true,
			loadingKirim : false,
		};
		
		this._isMounted = true;
		jenisPengaduan = this.props.navigation.state.params.jenisPengaduan;
		
		//Load SKPD Terkait----------------------------------------------------------------------------------------------------------------------
		fetch( URL_SQS + '/frontend/web/index.php?r=site/refskpd', {
			method: 'POST',
			headers: 
			{
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(
			{
				jenisPengaduan : this.props.navigation.state.params.jenisPengaduan
			})
		})
		.then((response) => response.json())
		.then((response) => {
			if(this._isMounted) {
				this.setState({
					itemsSKPD : response
				});
			}
		})
		.catch((error) => {
			alert('Koneksi bermasalah!');
		});
		//Load SKPD Terkait----------------------------------------------------------------------------------------------------------------------
	}
	
	componentDidMount() {
		const { navigation } = this.props;
		this.focusListener = navigation.addListener('didFocus', () => {
			//Set Perubahan Titik Koordinat------------------------------------------------------------------------------------------------------
			if(this.props.navigation.state.params.Lat)
			{	if(this._isMounted) {
					this.setState({
						latitude : this.props.navigation.state.params.Lat + '',
						longitude : this.props.navigation.state.params.Lon + '',
						loading : false,
					});
				}
			}
			else
			{	this.dapatkanLokasi();
			}
			//Set Perubahan Titik Koordinat------------------------------------------------------------------------------------------------------
			
			selesai = () => {
				if(this._isMounted) {
					this.setState({
						username : Username
					});
				}
			}
			db.transaction(function (txn) {
				txn.executeSql('select * from user', [], (tx, results) => {
					if(results.rows.length > 0) {
						Username = results.rows.item(0).username;
					}
				});
			}, null, selesai);
		});
	}
	
	componentWillUnmount() {
		this._isMounted = false;
	}
	
	selectedSKPD(selected) {
        if(this._isMounted) {
			this.setState({
				selectedItemSKPD : selected
			});
		}
    }
	
	dapatkanLokasi = () => {
		this.setState({ loading: true }, () => {
			try { SebastianManager.GetLocation(this.LocationResult); }
			catch(error) {
				if(this._isMounted) {
					this.setState({
						error: null,
						loading : false,
					});
				}
			}
		});
	}
	
	LocationResult = (result) => {
		var obj = JSON.parse(result);
		if(this._isMounted) {
			this.setState({
				latitude : obj.latitude + '',
				longitude : obj.longitude + '',
				error: null,
				loading : false,
			});
		}
		
		if(this.state.latitude == 'undefined' || this.state.longitude == 'undefined')
		{	this.setState({
				latitude : '',
				longitude : '',
			});
			alert("Lokasi anda tidak ditemukan, silahkan tekan tombol [Dapatkan Lokasi]");
		}
		else
		{	ToastAndroid.show("Lokasi anda ditemukan", ToastAndroid.SHORT);
		}
	}
	
	bukaMap(){
		const { navigate } = this.props.navigation;
		if(this.state.latitude == '' || this.state.longitude == '') {
			navigate('AmbilLokasi', { URL_SQS : URL_SQS, Lat : '2.958493', Lon : '99.064523'});
		}
		else {
			navigate('AmbilLokasi', { URL_SQS : URL_SQS, Lat : this.state.latitude, Lon : this.state.longitude});
		}
	}
	
	kirimPengaduan(){
		var cekSKPD = '';
		try {
			cekSKPD = this.state.selectedItemSKPD.Value + '';
		} catch (err) {}
		
		if(cekSKPD !== '' && this.state.latitude !== '' && this.state.longitude !== '' && this.state.imageSource !== null)
		{	if(this._isMounted) {
				this.setState({ loadingKirim : true });
			}
			fetch( URL_SQS + '/frontend/web/index.php?r=pengaduan/createandroid', {
				method : 'POST',
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json',
				},
				body : JSON.stringify({
					username : this.state.username,
					jenis_pengaduan : jenisPengaduan,
					id_dinas : this.state.selectedItemSKPD.Value,
					gambar : photo,
					kordinat_lat : this.state.latitude,
					kordinat_long : this.state.longitude,
					keterangan : this.state.keterangan,
				})
			})
			.then((response) => response.json())
			.then((response) => {
				if(this._isMounted) {
					this.setState({
						loadingKirim : false,
					});
				}
				ToastAndroid.show(response, ToastAndroid.SHORT);
				const { navigate } = this.props.navigation;
				navigate('Home');
			})
			.catch((error) => {
				if(this._isMounted) {
					this.setState({ loadingKirim : false });
				}
			});
		}
		else
		{	var msg = '';
			var n = 0;
			if(cekSKPD == '' || cekSKPD == 'undefined') {
				n++;
				msg += n + '. SKPD belum dipilih\n';
			}
			if(this.state.latitude == '' || this.state.longitude == '') {
				n++;
				msg += n + '. Koordinat anda Belum ditemukan, silahkan tekan tombol Dapatkan Lokasi\n';
			}
			if(this.state.imageSource == null) {
				n++;
				msg += n + '. Foto belum diambil\n';
			}
			alert(msg);
		}
	}
	
	selectPhoto(){
		ImagePicker.showImagePicker(optionsImage, (response) => {
			if (response.didCancel) {
				console.log('User cancelled image picker');
			}
			else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			}
			else {
				let source = { uri: response.uri };
				if(this._isMounted) {
					this.setState({
						imageSource : source,
					});
				}
				photo = 'data:image/jpeg;base64,' + response.data;
			}
		});
	}
	
	render() {
		const { navigation } = this.props;
		return (
			<ScrollView>
				<View style={[styles.luarAbu, styles.profilLuar]}>
					<Spinner visible={this.state.loadingKirim} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
					<View style={styles.profilLuarBiru}>
						<View style={{alignSelf: 'center', paddingTop : 38}}>
							<View style={styles.luarBtnPick}>
								<View style={styles.shadowBtnPick}> 
									<TouchableOpacity style={styles.btnPick} onPress = {() => this.dapatkanLokasi()}> 
										<Image style={{height : 20, width : 20}} source={require('../../../assets/lainnya/pick-map.png')} ></Image>
										<Text style={styles.btnNamePick}>Dapatkan Lokasi</Text>
									</TouchableOpacity>
								</View>
							</View>
							<View style={styles.luarBtnPick}>
								<View style={styles.shadowBtnPick}> 
									<TouchableOpacity style={styles.btnPick} onPress = {() => this.bukaMap()}> 
										<Image style={{height : 20, width : 20}} source={require('../../../assets/lainnya/buka-map.png')} ></Image>
										<Text style={styles.btnNamePick}>Dapatkan Buka Map</Text>
									</TouchableOpacity>
								</View>
							</View>
						</View>
						<View style={{alignSelf: 'center', paddingTop : 5, flexDirection: 'row'}}>
							<TextInput
								style = {styles.inputText}
								placeholder = "Latitude"
								underlineColorAndroid = "black"
								editable = {false}
								value = {this.state.latitude}
							/>
							<TextInput
								style = {styles.inputText}
								placeholder = "Longitude"
								underlineColorAndroid = "black"
								editable = {false}
								value = {this.state.longitude}
							/>
						</View>
					</View>
				</View>
				<View style={{marginHorizontal : 25}}>
					<PickerModal
						onSelected={(selected) => this.selectedSKPD(selected)}
						items={this.state.itemsSKPD}
						sortingLanguage={'tr'}
						showToTopButton={true}
						selected={this.state.selectedItemSKPD}
						autoGenerateAlphabeticalIndex={true}
						selectPlaceholderText={'Pilih'}
						searchPlaceholderText={'Cari...'}
						requireSelection={false}
						autoSort={false}
					/>
					<TouchableOpacity onPress = {this.selectPhoto.bind(this)}  style = {{ alignItems : 'center' }}>
						<Image
							style = {styles.gambar1}
							source = {this.state.imageSource !== null ? this.state.imageSource : require('../../../assets/lainnya/upload_image.png')}
						/>
						<Text>Pilih/Ambil Gambar</Text>
					</TouchableOpacity>
						
					<TextInput
						placeholder = "Keterangan"
						underlineColorAndroid = "black"
						onChangeText={(keterangan) => this.setState({keterangan})}
					/>			
				</View>
				<View style={styles.luarBtnLogout}>
					<TouchableOpacity style={styles.shadowBtnLogout} onPress = {() => this.kirimPengaduan()}> 
						<View style={styles.btnLogout}> 
							<Image style={{height : 20, width : 20}} source={require('../../../assets/lainnya/kirim.png')} ></Image>
							<Text style={styles.btnName}>Kirim Pengaduan</Text>
						</View>
					</TouchableOpacity>
				</View>
			</ScrollView>
		);
	}
}
