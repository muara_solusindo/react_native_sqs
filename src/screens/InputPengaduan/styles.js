import { StyleSheet } from 'react-native';
import { RecipeCard } from '../../AppStyles';

const styles = StyleSheet.create({
	container: RecipeCard.container,
	photo: RecipeCard.photo,
	photohome: RecipeCard.photohome,
	title: RecipeCard.title,
	category: RecipeCard.category,
	kotakLuar: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	kotakBiru : {
		backgroundColor: '#1ba0e1',
		height : 120,
	},
	luarAbu :{
		backgroundColor : '#f1f1f1', 
	},
	profilLuar :{
		height : 190
	},

	profilLuarBiru : {
		backgroundColor : '#1ba0e1', 
		height : 220, 
	},

	profilCard : {
		// flexDirection : 'row' , 
		backgroundColor : '#ffffff', 
		height : 50, 
		marginHorizontal : 20, 
		borderRadius : 10,
	},

	profilLuarImage  : {
		padding : 20,
		margin : 20 ,
		alignItems: 'center',
	},
	profilImage : {
		height : 95, 
		width : 95,
	},

	shadowprofilImage : {
		backgroundColor: 'white', width: 95, height: 95, borderRadius: 50,
		shadowColor: '#000',
		shadowOpacity: 1,
		elevation : 10,
	},

	profilAkun : {
		color : 'white',
		paddingTop : 12,
	},

	profilNama : {
		fontWeight : 'bold'
	},

	profilEmail : {
		fontSize : 13, 
		fontStyle: 'italic'
	},

	profilNoHp : {
		fontSize : 13
	},
	mainProfil : {
		width : `${100/3}%`
	},
	mainProfilTengah : {
		shadowColor: "#000000",
		shadowOpacity: 0.18,
		shadowRadius: 1,
		elevation : 2,
		marginTop : -1,
		marginBottom : -1,
		shadowOffset: {
			width: 0,
			height: 1,
		},

	},
	luarMainProfil : {
		paddingTop : 15, 
		alignItems : 'center', 
		justifyContent : 'center'
	},
	shadowImageProfil : {
		backgroundColor: 'white', 
		width: 40, 
		height: 40, 
		borderRadius: 40,
		shadowColor: '#000',
		shadowOpacity: 1,
		elevation : 6,
	},
	namaMainProfil : {
		fontSize : 12, 
		paddingTop : 10
	},
	luarInfromasiProfile : {
		marginTop : 130
	},
	boxInfromasiProfile : {
		flexDirection: 'row', 
		paddingLeft : 80,
		paddingBottom : 20,
	},
	infromasiProfilImage : {
		height : 30 ,
		width : 30 ,
		marginRight: 25,
	},
	informasiProfilNama : {
		fontSize : 15, 
		marginTop : 5,
	}, 
	italic : {
		fontStyle : 'italic'
	},
	luarBtnLogout : {
		marginTop : 10,
		alignItems : 'center',
		marginBottom : 20,

	},
	btnLogout : {
		height : 38,
		width : 200,
		backgroundColor: '#1ba0e1', 
		borderRadius : 10,
		alignItems : 'center',
		justifyContent : 'center',
		flexDirection : 'row',

	},
	btnName : {
		color : 'white',
		fontWeight : 'bold',
		paddingLeft : 8
	},
	shadowBtnLogout : {
		backgroundColor: 'white', 
		height : 38,
		width : 200, 
		borderRadius: 40,
		shadowColor: '#000',
		shadowOpacity: 1,
		elevation : 6,
	},
	
	inputText : {
		width : '45%',
		alignItems : 'center',
		borderColor: '#fff',
	},
	gambar1 : {
		width : 260,
		height : 260,
		marginTop : 10,
		alignItems: 'center'
	},
	luarBtnPick : {
		margin :5,
		alignItems : 'center',
	},
	btnPick : {
		height : 38,
		width : 250,
		backgroundColor: '#fff', 
		borderRadius : 10,
		alignItems : 'center',
		justifyContent : 'center',
		flexDirection : 'row',

	},
	btnNamePick : {
		color : '#1ba0e1',
		fontWeight : 'bold',
		paddingLeft : 8
	},
	shadowBtnPick : {
		backgroundColor: 'white', 
		height : 38,
		width : 250, 
		borderRadius: 40,
		shadowColor: '#000',
		shadowOpacity: 1,
		elevation : 6,
	},

});

export default styles;
