import React, {Component} from 'react';
import {
	AppRegistry,
	Text,
	View,
	AsyncStorage,
	StyleSheet,
	TouchableOpacity,
	Dimensions,
	Platform,
	ActivityIndicator,
} from 'react-native';

import MapView, { ProviderPropType, Marker, AnimatedRegion, Animated  } from 'react-native-maps';


let jenisPengaduan = null;

const screen = Dimensions.get('window');
const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE_DELTA = 0.0092;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

var markers = [
  {
    latitude: 1,
    longitude: 1,
    title: 'Foo Place',
    subtitle: '1234 Foo Drive'
  }
];

class AmbilLokasi extends Component{
	
	static navigationOptions = {
		header : null,
		headerMode: 'screen',
	};
	
	constructor(props){
		super(props);
		this.state = {
			lat : this.props.navigation.state.params.Lat * 1,
			lon : this.props.navigation.state.params.Lon * 1,
		}
	}
	
	pilihLokasi(event){
		this.setState({
			lat : event.nativeEvent.coordinate.latitude,
			lon : event.nativeEvent.coordinate.longitude,
		});
	}
	
	selesai() {
		const { navigate } = this.props.navigation;
		navigate('InputPengaduan', { Lat : this.state.lat, Lon : this.state.lon});
	}
	
	render() {
		return (
			<View style={styles.container}>
				<MapView
					onPress = {(value) => this.pilihLokasi(value)}
					provider={this.props.provider}
					style={styles.map}
					initialRegion={{
						latitude : this.state.lat,
						longitude : this.state.lon,
						latitudeDelta: LATITUDE_DELTA,
						longitudeDelta: LONGITUDE_DELTA,
					}}
				>
				
					<MapView.Marker
						coordinate={{
							latitude: this.state.lat,
							longitude: this.state.lon,
						}}
						title={"title"}
						description={"description"}
					/>
				
				</MapView>
				
				<View style={styles.buttonContainer}>
					<TouchableOpacity
						onPress={() => this.selesai()}
						style={[styles.bubble, styles.button]}
					>
						<Text>Selesai</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

AmbilLokasi.propTypes = {
	provider: ProviderPropType,
};

const styles = StyleSheet.create({
	container: {
		...StyleSheet.absoluteFillObject,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	map: {
		...StyleSheet.absoluteFillObject,
	},
	bubble: {
		flex: 1,
		backgroundColor: 'rgba(255,255,255,0.7)',
		paddingHorizontal: 18,
		paddingVertical: 12,
		borderRadius: 20,
	},
	latlng: {
		width: 200,
		alignItems: 'stretch',
	},
	button: {
		width: 80,
		paddingHorizontal: 12,
		alignItems: 'center',
		marginHorizontal: 10,
	},
	buttonContainer: {
		flexDirection: 'row',
		marginVertical: 20,
		backgroundColor: 'transparent',
	},
});

export default AmbilLokasi;