import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    // flexDirection: 'row',
    // alignItems: 'center',
	justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'flex-start',
    paddingHorizontal: 20
  },
  informasiAkun : {
	backgroundColor: '#1ba0e1',
	alignItems: 'center',
	padding : 20,
	marginBottom : 20,
  },
  imageSide  : {
	width : 80,
	height : 80
  },

  shadowimageSide : {
		backgroundColor: 'white', width: 80, height: 80, borderRadius: 40,
		shadowColor: '#000',
		shadowOpacity: 1,
		elevation : 10,
	},
});

export default styles;
