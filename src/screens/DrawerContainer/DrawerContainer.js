import React from 'react';
import { View, Image, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import MenuButton from '../../components/MenuButton/MenuButton';

export default class DrawerContainer extends React.Component {
  render() {
    const { navigation } = this.props;
    return (
		
      	<View style={styles.content}>
			<View style={styles.informasiAkun}>
				<View style={styles.shadowimageSide}>
					<Image style={styles.imageSide} source={require('../../../assets/icons/user.png')}/>
				</View>
				<Text style={{color : 'white'}}> {'\n'} Hallo, Administrator</Text>
			</View>
			<View style={styles.container}>
				<MenuButton
					title="HOME"
					source={require('../../../assets/sidebar/home.png')}
					onPress={() => {
					navigation.navigate('Home');
					navigation.closeDrawer();
					}}
				/>
				<MenuButton
					title="PENGURUSAN"
					source={require('../../../assets/sidebar/pengurusan.png')}
					onPress={() => {
					navigation.navigate('Pengurusan');
					// navigation.navigate('Categories');
					navigation.closeDrawer();
					}}
				/>
				
				<MenuButton
					title="CARI"
					source={require('../../../assets/sidebar/cari.png')}
					onPress={() => {
					navigation.navigate('Search');
					navigation.closeDrawer();
					}}
				/>
				{/* <MenuButton
					title="BANTUAN"
					source={require('../../../assets/sidebar/bantuan.png')}
					onPress={() => {
					navigation.navigate('Bantuan');
					navigation.closeDrawer();
					}}
				/> */}
				<MenuButton
					title="TENTANG"
					source={require('../../../assets/sidebar/tentang.png')}
					onPress={() => {
					navigation.navigate('Tentang');
					navigation.closeDrawer();
					}}
				/>

				<MenuButton
					title="AKUN SAYA"
					source={require('../../../assets/sidebar/profil.png')}
					onPress={() => {
					navigation.navigate('Profil');
					navigation.closeDrawer();
					}}
				/>
			</View>
      </View>
    );
  }
}

DrawerContainer.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired
  })
};
