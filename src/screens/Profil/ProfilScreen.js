import React , {Component}from 'react';
import {
  Text,
  View,
  Dimensions,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import BackButton from '../../components/BackButton/BackButton';
const{width : WIDTH} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';

import UrlServer from '../../UrlServer.js';
let URL_SQS = UrlServer.getUrl();
var SQLite = require('react-native-sqlite-storage');
const db = SQLite.openDatabase(UrlServer.getNamaDatabaseSQLite(), '1.0', '', 1);

class Menu extends Component {
	render(){
		return(
			<View style={styles.luarMainProfil}>
				<View style = {styles.shadowImageProfil}>
					<Image style={{height : 40, width : 40}} source={this.props.imgMenu}></Image>
				</View>
				<Text style={styles.namaMainProfil}>{this.props.namaMenu}</Text>
			</View>
		)
	}
}
class Informasi extends Component {
	render(){
		return(
			
			<View style={styles.boxInfromasiProfile}>
				<Image style={styles.infromasiProfilImage} source={this.props.imgInformasi}/>
				<Text style={styles.informasiProfilNama}>{this.props.namaInformasi} </Text>
			</View>
		)
	}
}
class InformasiEmail extends Component {
	render(){
		return(
			
			<View style={styles.boxInfromasiProfile}>
				<Image style={styles.infromasiProfilImage} source={this.props.imgInformasi}/>
				<Text style={[styles.informasiProfilNama, styles.italic]}>{this.props.namaInformasi} </Text>
			</View>
		)
	}
}

export default class ProfilScreen extends React.Component {
	
	static navigationOptions = ({ navigation }) => {
		return {
			headerTransparent: 'true',
			headerLeft: (
				<BackButton
					onPress={() => {
						navigation.goBack();
					}}
				/>
			)
		};
	};
	
	constructor(props){
		super(props);
		this.state = {
			username : null,
			nama : null,
			alamat : null,
			no_hp : null,
			isLoading : true,
		}
		this._isMounted = true;
		
		db.transaction((tx) => {
			tx.executeSql('SELECT * FROM user', [], (tx, results) => {
				if(this._isMounted) {
					this.setState({
						nama : results.rows.item(0).nama,
						username : results.rows.item(0).username,
						no_hp : results.rows.item(0).no_hp,
						isLoading : false,
					});
				}
			});
		});
	}
	
	componentWillUnmount() {
		this._isMounted = false;
	}
	
	keluar = () => {		
		selesai = () => {
			const { navigate } = this.props.navigation;
			navigate('Sqs');
		}
		db.transaction(function (txn) {
			txn.executeSql('delete from user', []);
		}, null, selesai);
	}
	
	render() {
		const { navigation } = this.props;
		return (
			<View style={{flex : 1}}>
				<ScrollView>
					<View style={[styles.blue]}/>
					<View style={styles.contents}>
						<View style={styles.profilLuarImage}>
							<View style={styles.shadowprofilImage}>
								<Image style={styles.profilImage} source={require('../../../assets/icons/user.png')}/>
							</View>
							<Text style={styles.profilAkun}> ADMINISTRATOR</Text>
						</View>
						<View style={styles.profilCard}>
							<View style={styles.mainProfil}>
								<View style={styles.luarMainProfil}>
									<View style = {styles.shadowImageProfil}>
										<TouchableOpacity onPress={() => {navigation.navigate('GantiPassword')}}>
										<Image style={{height : 40, width : 40}} source={require('../../../assets/profil/ubah-password.png')}></Image>
										</TouchableOpacity>
									</View>
									<Text style={styles.namaMainProfil}>Ubah Password</Text>
								</View>
							</View>
							<View style={styles.mainProfil}>
								<View style={styles.luarMainProfil}>
									<View style = {styles.shadowImageProfil}>
										<TouchableOpacity>
											<Image style={{height : 40, width : 40}} source={require('../../../assets/profil/ubah-profil.png')}></Image>
										</TouchableOpacity>
									</View>
									<Text style={styles.namaMainProfil}>Ubah Profil</Text>
								</View>
							</View>
							<View style={styles.mainProfil}>
								<View style={styles.luarMainProfil}>
									<View style = {styles.shadowImageProfil}>
										<TouchableOpacity >
											<Image style={{height : 40, width : 40}} source={require('../../../assets/profil/unggah-profil.png')}></Image>
										</TouchableOpacity>
									</View>
									<Text style={styles.namaMainProfil}>Unggah Profil</Text>
								</View>
							</View>
						</View>
						<View style={styles.luarInfromasiProfile}>
							<Informasi imgInformasi={require('../../../assets/profil/jabatan.png')} namaInformasi={this.state.username} />
							<Informasi imgInformasi={require('../../../assets/profil/nama.png')} namaInformasi={this.state.nama} />
							<InformasiEmail imgInformasi={require('../../../assets/profil/email.png')} namaInformasi="email@email.com" />
							<Informasi imgInformasi={require('../../../assets/profil/telepon.png')} namaInformasi={this.state.no_hp} />
						</View>
						<View>
							<TouchableOpacity
								style={styles.luarBtnLogout}
								onPress = {() => this.keluar()}
							>
								<View style={styles.shadowBtnLogout}> 
									<View style={styles.btnLogout}> 
										<Image style={{height : 20, width : 20}} source={require('../../../assets/profil/logout.png')} ></Image>
										<Text style={styles.btnName}>Logout</Text>
									</View>
								</View>
							</TouchableOpacity>
						</View>
					</View>
				</ScrollView>
			</View>
		);
	}
}































