import React from 'react';
import {
	FlatList,
	Text,
	View,
	Image,
	TouchableHighlight
} from 'react-native';
import styles from './styles';
import { ListItem, SearchBar } from 'react-native-elements';
import MenuImage from '../../components/MenuImage/MenuImage';

import UrlServer from '../../UrlServer.js';
let URL_SQS = UrlServer.getUrl();
var SQLite = require('react-native-sqlite-storage');
const db = SQLite.openDatabase(UrlServer.getNamaDatabaseSQLite(), '1.0', '', 1);

export default class SearchScreen extends React.Component {
	
	static navigationOptions = ({ navigation }) => {
		const { params = {} } = navigation.state;
		return {
			headerStyle: {
				backgroundColor: '#1ba0e1',
				elevation:7,
			},
			headerTintColor: 'white',
			headerRight: (
				<MenuImage
				onPress={() => {
					navigation.openDrawer();
				}}
				/>
			),
			headerTitle: (
				<SearchBar
				containerStyle={{
					backgroundColor: 'transparent',
					borderBottomColor: 'transparent',
					borderTopColor: 'transparent',
					flex: 1
				}}
				inputContainerStyle={{
					backgroundColor: '#EDEDED'
				}}
				inputStyle={{
					backgroundColor: '#EDEDED',
					borderRadius: 10,
					color: 'black'
				}}
				searchIcond
				clearIcon
				//lightTheme
				round
				onChangeText={text => params.handleSearch(text)}
				//onClear={() => params.handleSearch('')}
				placeholder="Cari"
				value={params.data}
				/>
			)
		};
	};
	
	
	constructor(props) {
		super(props);
		this.state = {
			value: '',
			data: [],
		};
		this._isMounted = true;
	}
	
	componentWillUnmount() {
		this._isMounted = false;
	}
	
	componentDidMount() {
		const { navigation } = this.props;
		navigation.setParams({
			handleSearch: this.handleSearch,
			data: this.getValue
		});
	}
	
	handleSearch = text => {
		if(text.length > 1)
		{	db.transaction((tx) => {
				tx.executeSql('SELECT * FROM user', [], (tx, results) => {
					setListView(results.rows.item(0).username);
				});
			});
			
			setListView = (Username) => {
				db.transaction((tx) => {
					var newState = [];
					tx.executeSql("select * from log_pengaduan where username = '" + Username + "' and (nama_dinas like '%" + text + "%'  or keterangan like '%" + text + "%') order by id_log desc", [], (tx, results) => {
						for(i=0; i<results.rows.length; i++) {
							var statusProsess = "Terkirim";
							if(results.rows.item(i).status_proses == "1")
							{	statusProsess = "Diterima";
							}
							if(results.rows.item(i).status_proses == "2")
							{	statusProsess = "Diteruskan";
							}
							if(results.rows.item(i).status_proses == "3")
							{	statusProsess = "Sedang Diproses";
							}
							if(results.rows.item(i).status_proses == "4")
							{	statusProsess = "Ditolak";
							}
							if(results.rows.item(i).status_proses == "5")
							{	statusProsess = "Selesai";
							}
							var valueToPush = {};
							valueToPush.key = results.rows.item(i).id_pengaduan + '';
							valueToPush.id_status_proses = results.rows.item(i).status_proses + '';
							valueToPush.gambar = results.rows.item(i).gambar + '';
							valueToPush.keterangan = results.rows.item(i).keterangan + '';
							valueToPush.tanggal = results.rows.item(i).created_at + '';
							valueToPush.status_proses = statusProsess;
							valueToPush.nama_yang_mengerjakan = results.rows.item(i).nama_yang_mengerjakan;
							valueToPush.no_hp_yang_mengerjakan = results.rows.item(i).no_hp_yang_mengerjakan;
							valueToPush.nama_dinas = results.rows.item(i).nama_dinas;
							valueToPush.kordinat_lat = results.rows.item(i).kordinat_lat;
							valueToPush.kordinat_long = results.rows.item(i).kordinat_long;
							newState.push(valueToPush);
						}
						if(results.rows.length > 0) {
							if(this._isMounted) {
								this.setState({
									data : newState,
								});
							}
						}
					});
				});
			}
			
		}
		else
		{	if(this._isMounted) {
				this.setState({
					data : [],
				});
			}
		}
	};

	getValue = () => {
		return this.state.value;
	};

	onPressPengaduan = item => {
		this.props.navigation.navigate('Pengaduan', { item });
	};

	renderPengaduans = ({ item }) => {
		var sttsProses = false;
		if(item.status_proses.toLowerCase() == "sedang diproses")
		{	sttsProses = true;
		}
		return (
			<TouchableHighlight underlayColor='rgba(73,182,77,1,0.9)' onPress={() => this.onPressPengaduan(item)}>
			<View style={styles.container}>
				<Image style={styles.photo} source={{ uri: URL_SQS + '/backend/web/uploads/' + item.gambar }} />
				<Text style={styles.title}>{item.nama_dinas}</Text>
				<Text style={styles.category}>{item.keterangan}</Text>
				<Text style={{textAlign : 'center'}}>Proses : {item.status_proses}</Text>
				{ (sttsProses) ? (
					<Text>{item.nama_yang_mengerjakan} ({item.no_hp_yang_mengerjakan})</Text>
				) : null }
			</View>
			</TouchableHighlight>
		);
	};

 	render() {
		return (
		<View>
			<FlatList
				vertical
				showsVerticalScrollIndicator={false}
				numColumns={2}
				data={this.state.data}
				renderItem={this.renderPengaduans}
				keyExtractor={item => `${item.pengaduanId}`}
				/>
		</View>
		);
  }
}
