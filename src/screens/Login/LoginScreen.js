import React , {Component}from 'react';
import {
  Text,
  View,
  Dimensions,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import BackButton from '../../components/BackButton/BackButton';
const{width : WIDTH} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';
import Spinner from 'react-native-loading-spinner-overlay';

import UrlServer from '../../UrlServer.js';
let URL_SQS = UrlServer.getUrl();
var SQLite = require('react-native-sqlite-storage');
const db = SQLite.openDatabase(UrlServer.getNamaDatabaseSQLite(), '1.0', '', 1);

export default class LoginScreen extends React.Component {
	static navigationOptions = {
		header: null
	};
	
	constructor(){
		super()
		this.state ={
			showPass : true,
			press : false,
			loading : false,
			userlogin : '',
		}
		this._isMounted = true;
	}
	
	componentDidMount() {
		const { navigation } = this.props;
		this.focusListener = navigation.addListener('didFocus', () => {
			db.transaction(function (txn) {
				txn.executeSql('select * from user', [], (tx, results) => {
					if(results.rows.length > 0) {
						goHome();
					}
				});
			});
			goHome = () => {
				const { navigate } = this.props.navigation;
				navigate('Home');
			}
		});
	}
	
	componentWillUnmount() {
		this._isMounted = false;
	}
	
	ketikUsername(text)
	{	if(this._isMounted) {
			this.setState({
				txtusername : text
			});
		}
	}
	
	ketikPassword(text)
	{	if(this._isMounted) {
			this.setState({
				txtpassword : text
			});
		}
	}
	
	daftar = () => {
		const { navigate } = this.props.navigation;
		navigate('Daftar');
	}
	
	goLupapassword = () => {
		const { navigate } = this.props.navigation;
		navigate('LupaPassword');
	}
	
	login = () => {
		if(this._isMounted) {
			this.setState({ loading : true });
		}
		fetch( URL_SQS + '/frontend/web/index.php?r=site/loginandroid', {
			method : 'POST',
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
			},
			body: JSON.stringify({
				txtusername : this.state.txtusername,
				txtpassword : this.state.txtpassword,
			})
		})
		.then((response) => response.json())
		.then((response) => {
			if(this._isMounted) {
				this.setState({ loading: false });
			}
			if(response[0].username)
			{	db.transaction(function (txn) {
					txn.executeSql('delete from user', []);
					txn.executeSql('insert into user (nama, username, no_hp) VALUES (:nama, :username, :no_hp)', [response[0].nama, response[0].username, response[0].no_hp]);
					txn.executeSql('delete from setting_mobile', []);
					txn.executeSql('insert into setting_mobile(nomor_hotline) VALUES (:nomor_hotline)', [response[0].nomor_hotline]);
				});
				const { navigate } = this.props.navigation;
				navigate('Home');
			}
			else
			{	alert('Username atau password salah!');
			}
		})
		.catch((error) => {
			if(this._isMounted) {
				this.setState({ loading: false });
			}
			alert('Username atau password salah!');
		});
	}

	showPass = () => {
		if(this.state.press == false){
			this.setState({ showPass : false, press : true})
		}else{
			this.setState({ showPass : true, press : false})
		}
	}
	onPressLupa = () => {
		this.props.navigation.navigate('LupaPassword');
	};
	
	render() {
		return (
			<View style={{flex : 1}}>
				<Spinner visible={this.state.loading} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
				<ScrollView>
					<View style={[styles.blue]}/>
					<View style={styles.contents}>
						<View style={styles.profilLuarImage}>
							<View style={styles.shadowprofilImage}>
								<Image style={styles.profilImage} source={require('../../../assets/icons/sqs-bulet.png')}/>
							</View>
							<Text style={styles.profilAkun}> Siantar Quick Service</Text>
						</View>
						<View style={styles.profilCard}>
							<View style={{justifyContent: 'center', alignItems: 'center', marginVertical : 20}}>
								<View>
									<Text style={{fontSize : 15, fontStyle : 'italic', marginBottom : 5}}>Masuk</Text>
								</View>
								<View style={styles.inputContainer}>
										<Icon  name={'ios-person'}  size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
										<TextInput 
											style={styles.input}
											placeholder={'Username'}
											placeholderTextColor={'rgba(255,255,0.7)'}
											underlineColorAndroid = 'transparent'
											onChangeText={(txtusername) => this.setState({txtusername})}
										/>
								</View>
								<View style={styles.inputContainer}>
									<Icon name={'ios-lock'}  size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
									<TextInput 
										style={styles.input}
										placeholder={'Password'}
										placeholderTextColor={'rgba(255,255,0.7)'}
										underlineColorAndroid = 'transparent'
										secureTextEntry={this.state.showPass}
										onChangeText = {(text) => this.ketikPassword(text)}
									/>
									<TouchableOpacity style={styles.btnEye} onPress={this.showPass.bind(this)}>
										<Icon name={this.state.showPass == false ? 'ios-eye' :  'ios-eye-off'} size={26} colot={'rgba(255,255,255,0.7)'} />
									</TouchableOpacity>
								</View>
								<TouchableHighlight  underlayColor='rgba(73,182,77,1,0.9)' onPress={() => this.props.navigation.navigate('LupaPassword')}>
									<View>
											<Text style={{color : '#249cd6', paddingTop : 9}}> Lupa Password ?</Text>
									</View>
								</TouchableHighlight>

								<View>
									<TouchableOpacity
										style={styles.luarBtnLogout}
										onPress = { this.login } >
										<View style={styles.shadowBtnLogout}> 
											<View style={styles.btnLogout}> 
												<Text style={styles.btnName}>Masuk</Text>
											</View>
										</View>
									</TouchableOpacity>
								</View>
								
							</View>
						</View>
					</View>

				</ScrollView>
			</View>
		);
	}
}
