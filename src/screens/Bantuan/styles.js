import { StyleSheet } from 'react-native';
import { RecipeCard } from '../../AppStyles';

const styles = StyleSheet.create({
	container: RecipeCard.container,
	photo: RecipeCard.photo,
	photohome: RecipeCard.photohome,
	title: RecipeCard.title,
	category: RecipeCard.category,
});

export default styles;
