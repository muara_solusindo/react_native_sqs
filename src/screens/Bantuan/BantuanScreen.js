import React from 'react';
import {
  Text,
  View,
} from 'react-native';
import styles from './styles';

export default class PengurusanScreen extends React.Component {
  static navigationOptions = {
	title: 'Butuh Bantuan ?',
	headerStyle: {
		backgroundColor: '#1ba0e1',
		elevation:7,
	},
	headerTitleStyle: { 
		color: 'white',
		fontWeight: 'normal', 
	},
	headerTintColor: 'white',

  };


  render() {
    return (
      <View style={{alignItems : 'center', justifyContent : 'center', flex : 1}}>
		<Text> Disini FAQ / Bantuan </Text>
      </View>
    );
  }
}
