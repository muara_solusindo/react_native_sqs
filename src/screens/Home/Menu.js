import React from 'react';
import { TouchableHighlight, Image, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

export default class Menu extends React.Component {
  render() {
    return (
    //   <TouchableHighlight
    //     onPress={this.props.onPress}
    //     style={styles.btnClickContain}
    //     underlayColor="rgba(128, 128, 128, 0.1)"
    //   >
    //     <View style={styles.btnContainer}>
    //       <Image source={this.props.source} style={styles.btnIcon} />
    //       <Text style={styles.btnText}>{this.props.title}</Text>
    //     </View>
    //   </TouchableHighlight>
	  <View style={styles.menuKotak}>
			<TouchableHighlight 
				underlayColor='rgba(73,182,77,1,0.9)' 
				onPress={this.props.onPress}
				// onPress={() => this.props.navigation.navigate('InputPengaduan')}
			>
				<View style={styles.menuKotakGambar}>
					<View style={styles.shadowMenuGambar}>
						<Image style={styles.menuGambar} source={this.props.imgMenu}/>
					</View>
				</View>
			</TouchableHighlight> 
			<Text style={styles.menuNama}>{this.props.namaMenu}</Text>
		</View>
	);
	
  }
}

Menu.propTypes = {
  onPress: PropTypes.func,
  source: PropTypes.number,
  title: PropTypes.string
};
