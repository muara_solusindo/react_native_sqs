import { StyleSheet } from 'react-native';
import { RecipeCard } from '../../AppStyles';

const styles = StyleSheet.create({
	container: RecipeCard.container,
	photo: RecipeCard.photo,
	photohome: RecipeCard.photohome,
	title: RecipeCard.title,
	category: RecipeCard.category,

  	luarAbu :{
		backgroundColor : '#f1f1f1', 
	},
	profilLuar :{
		height : 120
	},

	profilLuarBiru : {
		backgroundColor : '#1ba0e1', 
		height : 100, 
		borderBottomRightRadius : 7, 
		borderBottomLeftRadius : 7
	},

	profilCard : {
		flexDirection : 'row' , 
		backgroundColor : '#ffffff', 
		height : 115, 
		marginHorizontal : 10, 
		borderRadius : 7,
		shadowColor: "#000000",
		shadowOpacity: 0.9,
		shadowRadius: 6,
		elevation : 3.5
	},

	profilImage : {
		height : 70, 
		width : 70
	},

	shadowprofilImage : {
		backgroundColor: 'white', width: 70, height: 70, borderRadius: 35,
		shadowColor: '#000',
		shadowOpacity: 1,
		elevation : 10,
	},

	profilNama : {
		fontWeight : 'bold'
	},

	profilEmail : {
		fontSize : 13, 
		fontStyle: 'italic'
	},

	profilNoHp : {
		fontSize : 13
	},

	pengaduanLuarTitle : {
		flexDirection : 'row',  
		justifyContent: 'space-between', 
		marginHorizontal : 15
	},
	pengaduanTitle : {
		fontSize : 15, 
		fontWeight : 'bold',
		color : '#1C1C1C'
	},

	pengaduanLihatSemua : {
		fontSize : 13 , 
		fontWeight : 'bold', 
		color : '#1ba0e1'
	},

	pengaduanLuarCard : {
		flexDirection : 'row', 
		paddingBottom : 7 , 
		// paddingLeft : 7 
	},

	pengaduanCard : {
		marginHorizontal : 6,
		marginVertical : 6,
		backgroundColor : 'white', 
		borderRadius : 6,
		shadowColor: "#000000",
		shadowOpacity: 0.9,
		shadowRadius: 6,
		elevation : 3.5
	},
	pengaduanBukaTutup : {
		padding : 3
	},

	pengaduanCardLuarImage : {
			width : 280, 
			height : 120, 
			borderTopLeftRadius : 6, 
			borderTopRightRadius : 6
	},

	pengaduanCardImage : {
		height : undefined, 
		width : undefined, 
		resizeMode : 'cover', 
		flex : 1 , 
		borderTopLeftRadius : 6, 
		borderTopRightRadius : 6
	},

	pengaduanCardTitle  : {
		marginTop : 6,
		fontSize : 14, 
		fontWeight : 'bold', 
		color : '#1C1C1C'
	},
	pengaduanCardKeterangan : {
		marginTop : 3,
		fontSize : 12, 
		color : '#1C1C1C',
		marginLeft : 5
	},

	menuLuar : {
		flexDirection : 'row',
		flexWrap: 'wrap',
		// marginHorizontal : 15,
		marginTop : 10

	},
	menuPerBaris : {
		justifyContent : 'space-between',
		flexDirection : 'row',
		width : '100%',
		marginBottom : 18 

	},

	menuKotak : {
		width : '25%',
		alignItems : 'center',
	},

	menuKotakGambar : {
		width : 53, 
		height : 53, 
		justifyContent: 'center',	
		alignItems: 'center',
	},

	menuNama : {
		fontSize : 11,
		// fontWeight : 'bold',
		textAlign : 'center',
		marginTop : 6,
		color : '#929292',
		flexWrap: 'wrap',
		width : '95%',
	},

	menuGambar : {
		height : 53,
		width : 53
	},
	shadowMenuGambar :{
		backgroundColor: 'white', width: 53, height: 53, borderRadius: 28,
		shadowColor: '#000',
		shadowOpacity: 1,
		elevation : 6,
	},
	padd10 : {
		padding : 10
	},

	padd5 : {
		padding : 5
	},

	paddtop5 : {
		paddingTop : 5
	}



});

export default styles;
