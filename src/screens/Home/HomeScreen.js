import React, {Component} from 'react';
import { FlatList, ScrollView, Text, View, TouchableHighlight, Image, StyleSheet} from 'react-native';
import styles from './styles';
import MenuImage from '../../components/MenuImage/MenuImage';
import { pengaduanterbarus } from '../../data/dataPengaduan';
import { getCategoryName } from '../../data/MockDataAPI2';
import Menu from './Menu';

import UrlServer from '../../UrlServer.js';
let URL_SQS = UrlServer.getUrl();
var SQLite = require('react-native-sqlite-storage');
const db = SQLite.openDatabase(UrlServer.getNamaDatabaseSQLite(), '1.0', '', 1);

export default class HomeScreen extends React.Component {
	
	static navigationOptions = ({ navigation }) => ({
		headerStyle: {
			backgroundColor: '#1ba0e1',
			elevation:0
		},
		headerLeft: (
		  <MenuImage
			onPress={() => {
			  navigation.openDrawer();
			}}
		  />
		),
		headerRight: (
			<Image style={{
				width: 50,
				height: 25,
				marginVertical: 6,
				marginHorizontal : 15
			}} 
			source={require('../../../assets/icons/aplikasi.png')}/>
		)
	});

	constructor(props) {
		super(props);
		this.state = {
			username : null,
			nama : null,
			alamat : null,
			no_hp : null,
			dataSource : null,
			id_log_terakhir : '',
		}
		this._isMounted = true;
		
		//Confirm Enable GPS-------------------------------------------------------------------------------------------------------------------
		try { navigator.geolocation.getCurrentPosition(function () {}, function () {}, {}); }
		catch(err) {}
		//Confirm Enable GPS-------------------------------------------------------------------------------------------------------------------
		
	}
	
	componentDidMount() {
		const { navigation } = this.props;
		this.focusListener = navigation.addListener('didFocus', () => {
			
			db.transaction((tx) => {
				tx.executeSql('SELECT * FROM user', [], (tx, results) => {
					if(this._isMounted) {
						this.setState({
							nama : results.rows.item(0).nama,
							username : results.rows.item(0).username,
							no_hp : results.rows.item(0).no_hp,
						});
					}
					setListView(results.rows.item(0).username);
				});
			});
			
			setListView = (Username) => {
				db.transaction((tx) => {
					tx.executeSql("select max(id_log) as id_log from log_pengaduan where username = '" + Username + "'", [], (tx, results) => {
						if(this._isMounted) {
							this.setState({
								id_log_terakhir : results.rows.item(0).id_log * 1,
							});
						}
						LoadDataDariServer(this.state.id_log_terakhir);
					});
					
					var newState = [];
					tx.executeSql("select * from log_pengaduan where username = '" + Username + "' order by id_log desc limit 3", [], (tx, results) => {
						for(i=0; i<results.rows.length; i++) {
							var statusProsess = "Terkirim";
							if(results.rows.item(i).status_proses == "1")
							{	statusProsess = "Diterima";
							}
							if(results.rows.item(i).status_proses == "2")
							{	statusProsess = "Diteruskan";
							}
							if(results.rows.item(i).status_proses == "3")
							{	statusProsess = "Sedang Diproses";
							}
							if(results.rows.item(i).status_proses == "4")
							{	statusProsess = "Ditolak";
							}
							if(results.rows.item(i).status_proses == "5")
							{	statusProsess = "Selesai";
							}
							var valueToPush = {};
							valueToPush.key = results.rows.item(i).id_pengaduan + '';
							valueToPush.id_status_proses = results.rows.item(i).status_proses + '';
							valueToPush.gambar = results.rows.item(i).gambar + '';
							valueToPush.keterangan = results.rows.item(i).keterangan + '';
							valueToPush.tanggal = results.rows.item(i).created_at + '';
							valueToPush.status_proses = statusProsess;
							valueToPush.nama_yang_mengerjakan = results.rows.item(i).nama_yang_mengerjakan;
							valueToPush.no_hp_yang_mengerjakan = results.rows.item(i).no_hp_yang_mengerjakan;
							valueToPush.nama_dinas = results.rows.item(i).nama_dinas;
							valueToPush.kordinat_lat = results.rows.item(i).kordinat_lat;
							valueToPush.kordinat_long = results.rows.item(i).kordinat_long;
							newState.push(valueToPush);
						}
						if(results.rows.length > 0) {
							if(this._isMounted) {
								this.setState({
									dataSource : newState,
								});
							}
						}
					});
				});
			}
		});
		
		LoadDataDariServer = (id) => {
			fetch( URL_SQS + '/frontend/web/index.php?r=pengaduan/ceklog', {
				method : 'POST',
				headers : {
					'Accept': 'application/json',
					'Content-Type': 'application/json',
				},
				body : JSON.stringify({
					username : this.state.username,
					id_log_terakhir : this.state.id_log_terakhir
				})
			})
			.then((response) => response.json())
			.then((response) => {
				
				selesai = () => {
					setListView(this.state.username);
				}
				
				db.transaction(function (txn) {
					for(i=0; i<response.length; i++)
					{	txn.executeSql(response[i].sql, []);
					}
				}, null, selesai);
			});
		}
		
	}
	
	componentWillUnmount() {
		this._isMounted = false;
	}

	onPressPengaduan = item => {
		this.props.navigation.navigate('Pengaduan', { item });
	};

	renderPengaduans = ({ item }) => {
		var sttsProses = false;
		if(item.status_proses.toLowerCase() == "sedang diproses")
		{	sttsProses = true;
		}
		return (
			<TouchableHighlight underlayColor='rgba(73,182,77,1,0.9)' onPress={() => this.onPressPengaduan(item)}>
				<View  style={styles.pengaduanCard}>
					<View style={styles.pengaduanCardLuarImage}>
						<Image style={styles.pengaduanCardImage} source={{ uri: URL_SQS + '/backend/web/uploads/' + item.gambar }}/>
					</View>
					<View style={styles.padd5}>
						<Text style={{ textAlign : 'center', fontWeight : 'bold' }}>{item.nama_dinas}</Text>
						<Text style={{ textAlign : 'center' }}>{item.keterangan}</Text>
						<Text style={{ textAlign : 'center' }}>Proses : {item.status_proses}</Text>
					</View>
					{ (sttsProses) ? (
						<View>
							<Text>{item.nama_yang_mengerjakan} ({item.no_hp_yang_mengerjakan})</Text>
						</View>
					) : null }
				</View>
			</TouchableHighlight>
		);
	};


  render() {
	const { navigation } = this.props;
    return (
      <View style={{flex : 1}}>
	  	{/* Profil */}
	  	<View style={[styles.luarAbu, styles.profilLuar]}>
			<View style={styles.profilLuarBiru}>
				<View style={styles.profilCard}>
					<View style={styles.padd10}>
						<View style={styles.shadowprofilImage}>
							<Image style={styles.profilImage} source={require('../../../assets/icons/user.png')}/>
						</View>
					</View>
					<View style={styles.padd10}>
						<Text> {this.state.username}</Text>
						<Text style={styles.profilNama}> {this.state.nama} </Text>
						<Text style={styles.profilEmail}></Text>
						<Text style={styles.profilNoHp}> {this.state.no_hp}</Text>
					</View>
				</View>
			</View>
		</View>

		<ScrollView>
			{/* Pengaduan Terbaru */}
			<View style={styles.luarAbu}>
				<View style={styles.pengaduanLuarTitle}>
					<Text style={styles.pengaduanTitle}>Pengaduan Terbaru</Text>
					{/* <LihatLainnya/> */}
					<TouchableHighlight  onPress={() => this.props.navigation.navigate('Pengurusan')}>
						<View>
							<Text style={styles.pengaduanLihatSemua}>Lihat Lainnya >  </Text>
						</View>
					</TouchableHighlight>
				</View>
				<ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={styles.pengaduanLuarCard}>
					{/* List Pengaduan Terbaru */}
						<View style={styles.pengaduanBukaTutup}></View>
							<FlatList
								data={this.state.dataSource}
								horizontal={true}
								renderItem={this.renderPengaduans}
								keyExtractor={item => `${item.pengaduanId}`}
							/>
						<View style={styles.pengaduanBukaTutup}></View>
				</ScrollView>
			</View>
			
			{/* Menu */}
			<View style={styles.menuLuar}>
				{/* Baris 1 */}
				<View style={styles.menuPerBaris}>
					<Menu imgMenu={require('../../../assets/menu/pelanggaran-perda.png')} namaMenu="Pelanggaran Perda" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '1'})}}/>
					<Menu imgMenu={require('../../../assets/menu/sampah.png')} namaMenu="Sampah" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '2'})}}/>
					<Menu imgMenu={require('../../../assets/menu/sekolah.png')} namaMenu="Sekolah" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '3'})}}/>
					<Menu imgMenu={require('../../../assets/menu/pungli.png')} namaMenu="Pungli" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '4'})}}/>
				</View>
				
				{/* Baris 2 */}
				<View style={styles.menuPerBaris}>
					<Menu imgMenu={require('../../../assets/menu/lampu-jalan.png')} namaMenu="Lampu Jalan" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '5'})}}/>
					<Menu imgMenu={require('../../../assets/menu/pohon-tumbang.png')} namaMenu="Pohon Tumbang" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '6'})}}/>
					<Menu imgMenu={require('../../../assets/menu/kebakaran.png')} namaMenu="Kebakaran" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '7'})}}/>
					<Menu imgMenu={require('../../../assets/menu/konflik.png')} namaMenu="Konflik" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '8'})}}/>
				</View>
				
				{/* Baris 3 */}
				<View style={styles.menuPerBaris}>
					<Menu imgMenu={require('../../../assets/menu/pelajar.png')} namaMenu="Pelajar" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '9'})}}/>
					<Menu imgMenu={require('../../../assets/menu/kemacetan.png')} namaMenu="Kemacetan" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '10'})}}/>
					<Menu imgMenu={require('../../../assets/menu/kecelakaan.png')} namaMenu="Kecelakaan" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '11'})}}/>
					<Menu imgMenu={require('../../../assets/menu/mrx.png')} namaMenu="Mr. X" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '12'})}}/>
				</View>
				
				{/* Baris 4 */}
				<View style={styles.menuPerBaris}>
					<Menu imgMenu={require('../../../assets/menu/banjir.png')} namaMenu="Banjir" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '13'})}}/>
					<Menu imgMenu={require('../../../assets/menu/longsor.png')} namaMenu="Longsor" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '14'})}}/>
					<Menu imgMenu={require('../../../assets/menu/pms.png')} namaMenu="PMS" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '15'})}}/>
					<Menu imgMenu={require('../../../assets/menu/kesehatan.png')} namaMenu="Kesehatan" onPress={() => {navigation.navigate('InputPengaduan', {jenisPengaduan : '16'})}}/>
					
				</View>
			</View>

			{/* Test Link Terbaru */}
			{/* <View style={[styles.luarAbu, styles.paddtop5]}>
				<View style={styles.pengaduanLuarTitle}>
					<Text style={styles.pengaduanTitle}>Informasi Terbaru</Text>
					<TouchableHighlight  onPress={() => this.props.navigation.navigate('Pengurusan')}>
						<View>
							<Text style={styles.pengaduanLihatSemua}>Lihat Lainnya >  </Text>
						</View>
					</TouchableHighlight>
				</View>
				<ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={styles.pengaduanLuarCard}>
					<View style={styles.pengaduanBukaTutup}></View>
						<Pengaduan imgPengaduan={require('../../../assets/pengaduan/pengaduan1.jpg')} pengaduanJudul="Pengaduan 1" pengaduanKeterangan="Keterangan Disini"/>
						<Pengaduan imgPengaduan={require('../../../assets/pengaduan/pengaduan2.jpg')} pengaduanJudul="Pengaduan 2" pengaduanKeterangan="Keterangan Disini"/>
						<Pengaduan imgPengaduan={require('../../../assets/pengaduan/pengaduan3.jpg')} pengaduanJudul="Pengaduan 3" pengaduanKeterangan="Keterangan Disini"/>
					<View style={styles.pengaduanBukaTutup}></View>
				</ScrollView>
			</View> */}
      	</ScrollView>
      </View>
    );
  }
}