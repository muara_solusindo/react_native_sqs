import React from 'react';
import {
	FlatList,
	Text,
	View,
	Image,
	TouchableHighlight,
	ScrollView, 
	StyleSheet,
	ActivityIndicator,
	Dimensions,
} from 'react-native';
import styles from './styles';
import { pengaduans } from '../../data/dataPengaduan';
import { getCategoryName } from '../../data/MockDataAPI2';

import UrlServer from '../../UrlServer.js';
let URL_SQS = UrlServer.getUrl();
var SQLite = require('react-native-sqlite-storage');
const db = SQLite.openDatabase(UrlServer.getNamaDatabaseSQLite(), '1.0', '', 1);

let deviceHeight = Dimensions.get('window').height;
var ContentHeight = 0;

export default class PengurusanScreen extends React.Component {
	
	static navigationOptions = {
		title: 'Pengurusan',
		headerStyle: {
			backgroundColor: '#1ba0e1',
			elevation:7,
		},
		headerTitleStyle: { 
			color: 'white',
			fontWeight: 'normal', 
		},
		headerTintColor: 'white',
	};
	
	constructor(props) {
		super(props);
		
		this.state = {
			username : null,
			dataSource : [],
			isLoading : false,
			id_log_terakhir : 0,
			limit : 10,
			id_log_terendah : null,
		}
		this._isMounted = true;
		
		loadData = () => {
			db.transaction(function (txn) {
				txn.executeSql('select * from user', [], (tx, results) => {
					if(results.rows.length > 0) {
						Username = results.rows.item(0).username;
					}
					setListView(Username);
				});
			});
			setListView = (Username) => {
				if(this._isMounted) {
					this.setState({
						username : Username
					});
				}
				db.transaction((tx) => {
					tx.executeSql("select max(id_log) as id_log from log_pengaduan where username = '" + Username + "'", [], (tx, results) => {
						if(this._isMounted) {
							this.setState({
								id_log_terakhir : results.rows.item(0).id_log * 1,
							});
						}
					});
					var newState = [];
					tx.executeSql("select * from log_pengaduan where username = '" + Username + "' order by id_log desc limit " + this.state.limit, [], (tx, results) => {
						for(i=0; i<results.rows.length; i++) {
							var statusProsess = "Terkirim";
							if(results.rows.item(i).status_proses == "1")
							{	statusProsess = "Diterima";
							}
							if(results.rows.item(i).status_proses == "2")
							{	statusProsess = "Diteruskan";
							}
							if(results.rows.item(i).status_proses == "3")
							{	statusProsess = "Sedang Diproses";
							}
							if(results.rows.item(i).status_proses == "4")
							{	statusProsess = "Ditolak";
							}
							if(results.rows.item(i).status_proses == "5")
							{	statusProsess = "Selesai";
							}
							var valueToPush = {};
							valueToPush.key = results.rows.item(i).id_pengaduan + '';
							valueToPush.id_status_proses = results.rows.item(i).status_proses + '';
							valueToPush.gambar = results.rows.item(i).gambar + '';
							valueToPush.keterangan = results.rows.item(i).keterangan + '';
							valueToPush.tanggal = results.rows.item(i).created_at + '';
							valueToPush.status_proses = statusProsess;
							valueToPush.nama_yang_mengerjakan = results.rows.item(i).nama_yang_mengerjakan;
							valueToPush.no_hp_yang_mengerjakan = results.rows.item(i).no_hp_yang_mengerjakan;
							valueToPush.nama_dinas = results.rows.item(i).nama_dinas;
							valueToPush.kordinat_lat = results.rows.item(i).kordinat_lat;
							valueToPush.kordinat_long = results.rows.item(i).kordinat_long;
							newState.push(valueToPush);
						}
						if(results.rows.length > 0) {
							if(this._isMounted) {
								this.setState({
									dataSource : newState,
									id_log_terendah : results.rows.item(results.rows.length - 1).id_log,
								});
							}
						}
					});
				});
				
				//Cek log pengaduan dari server--------------------------------------------------------------------------------
				fetch( URL_SQS + '/frontend/web/index.php?r=pengaduan/ceklog', {
					method : 'POST',
					headers : {
						'Accept': 'application/json',
						'Content-Type': 'application/json',
					},
					body : JSON.stringify({
						username : Username,
						id_log_terakhir : this.state.id_log_terakhir
					})
				})
				.then((response) => response.json())
				.then((response) => {
					
					selesai = () => {
						if(response.length > 0)
						{	db.transaction((tx) => {
								tx.executeSql("select max(id_log) as id_log from log_pengaduan where username = '" + Username + "'", [], (tx, results) => {
									if(this._isMounted) {
										this.setState({
											id_log_terakhir : results.rows.item(0).id_log * 1,
										});
									}
								});
								var newState = [];
								tx.executeSql("select * from log_pengaduan where username = '" + Username + "' order by id_log desc limit " + this.state.limit, [], (tx, results) => {
									for(i=0; i<results.rows.length; i++) {
										var statusProsess = "Terkirim";
										if(results.rows.item(i).status_proses == "1")
										{	statusProsess = "Diterima";
										}
										if(results.rows.item(i).status_proses == "2")
										{	statusProsess = "Diteruskan";
										}
										if(results.rows.item(i).status_proses == "3")
										{	statusProsess = "Sedang Diproses";
										}
										if(results.rows.item(i).status_proses == "4")
										{	statusProsess = "Ditolak";
										}
										if(results.rows.item(i).status_proses == "5")
										{	statusProsess = "Selesai";
										}
										var valueToPush = {};
										valueToPush.key = results.rows.item(i).id_pengaduan + '';
										valueToPush.id_status_proses = results.rows.item(i).status_proses + '';
										valueToPush.gambar = results.rows.item(i).gambar + '';
										valueToPush.keterangan = results.rows.item(i).keterangan + '';
										valueToPush.tanggal = results.rows.item(i).created_at + '';
										valueToPush.status_proses = statusProsess;
										valueToPush.nama_yang_mengerjakan = results.rows.item(i).nama_yang_mengerjakan;
										valueToPush.no_hp_yang_mengerjakan = results.rows.item(i).no_hp_yang_mengerjakan;
										valueToPush.nama_dinas = results.rows.item(i).nama_dinas;
										valueToPush.kordinat_lat = results.rows.item(i).kordinat_lat;
										valueToPush.kordinat_long = results.rows.item(i).kordinat_long;
										newState.push(valueToPush);
									}
									if(results.rows.length > 0) {
										if(this._isMounted) {
											this.setState({
												dataSource : newState,
												id_log_terendah : results.rows.item(results.rows.length - 1).id_log * 1,
											});
										}
									}
								});
							});
						}
					}
					db.transaction(function (txn) {
						for(i=0; i<response.length; i++)
						{	txn.executeSql(response[i].sql, []);
						}
					}, null, selesai);
				});
			}
			
		}
		loadData();
	}
	
	componentDidMount() {
		const { navigation } = this.props;
		this.focusListener = navigation.addListener('didFocus', () => {
			if(this._isMounted) {
				this.setState({ isLoading : false });
			}
			ContentHeight = 0;
			loadData();
		});
	}
	
	componentWillUnmount() {
		this._isMounted = false;
	}
	
	handleScroll = (event) => {
		ContentHeight = event.nativeEvent.contentSize.height - event.nativeEvent.layoutMeasurement.height;
		if(event.nativeEvent.contentOffset.y >= ContentHeight)
		{	if(this._isMounted) {
				this.setState({
					isLoading : true
				});
			}
			db.transaction((tx) => {
				var newState = [];
				tx.executeSql("select * from log_pengaduan where username = '" + this.state.username + "' and id_log < " + this.state.id_log_terendah + " order by id_log desc limit " + this.state.limit, [], (tx, results) => {
					for(i=0; i<results.rows.length; i++) {
						var statusProsess = "Terkirim";
						if(results.rows.item(i).status_proses == "1")
						{	statusProsess = "Diterima";
						}
						if(results.rows.item(i).status_proses == "2")
						{	statusProsess = "Diteruskan";
						}
						if(results.rows.item(i).status_proses == "3")
						{	statusProsess = "Sedang Diproses";
						}
						if(results.rows.item(i).status_proses == "4")
						{	statusProsess = "Ditolak";
						}
						if(results.rows.item(i).status_proses == "5")
						{	statusProsess = "Selesai";
						}
						var valueToPush = {};
						valueToPush.key = results.rows.item(i).id_pengaduan + '';
						valueToPush.id_status_proses = results.rows.item(i).status_proses + '';
						valueToPush.gambar = results.rows.item(i).gambar + '';
						valueToPush.keterangan = results.rows.item(i).keterangan + '';
						valueToPush.tanggal = results.rows.item(i).created_at + '';
						valueToPush.status_proses = statusProsess;
						valueToPush.nama_yang_mengerjakan = results.rows.item(i).nama_yang_mengerjakan;
						valueToPush.no_hp_yang_mengerjakan = results.rows.item(i).no_hp_yang_mengerjakan;
						valueToPush.nama_dinas = results.rows.item(i).nama_dinas;
						valueToPush.kordinat_lat = results.rows.item(i).kordinat_lat;
						valueToPush.kordinat_long = results.rows.item(i).kordinat_long;
						this.state.dataSource.push(valueToPush);
					}
					if(results.rows.length > 0) {
						if(this._isMounted) {
							this.setState({
								id_log_terendah : results.rows.item(results.rows.length - 1).id_log,
								isLoading : false
							});
						}
					}
				});
				tx.executeSql("select count(*) as jumlah from log_pengaduan where username = '" + this.state.username + "'", [], (tx, results) => {
					if(results.rows.item(0).jumlah == this.state.dataSource.length){
						if(this._isMounted) {
							this.setState({
								isLoading : false
							});
						}
					}
				});
			});
			//Tidak loading saat scroll ke atas
			// ContentHeight = event.nativeEvent.contentOffset.y;
		}
		
	}
	
	onPressPengaduan = item => {
		this.props.navigation.navigate('Pengaduan', { item });
	};
	
	renderPengaduans = ({ item }) => {
		var sttsProses = false;
		if(item.status_proses.toLowerCase() == "sedang diproses")
		{	sttsProses = true;
		}
		return (
			<TouchableHighlight underlayColor='rgba(73,182,77,1,0.9)' onPress={() => this.onPressPengaduan(item)}>
				<View style={styles.container}>
					<Image style={styles.photohome} source = {{ uri : URL_SQS + '/backend/web/uploads/' + item.gambar }} />
					<Text style={{ textAlign : 'center', fontWeight : 'bold' }}>{item.nama_dinas}</Text>
					<Text style={{ textAlign : 'center' }}>{item.keterangan}</Text>
					<Text style={{ textAlign : 'center' }}>Proses : {item.status_proses}</Text>
					{ (sttsProses) ? (
						<View>
							<Text>{item.nama_yang_mengerjakan} ({item.no_hp_yang_mengerjakan})</Text>
						</View>
					) : null }
				</View>
			</TouchableHighlight>
		);
	};
	
	render() {
		return (
			<View>
				<FlatList
					vertical
					showsVerticalScrollIndicator={false}
					numColumns={2}
					data={this.state.dataSource}
					renderItem={this.renderPengaduans}
					onScroll={this.handleScroll}
				/>
			</View>
		);
	}
	
}
