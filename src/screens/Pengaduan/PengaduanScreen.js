import React from 'react';
import {
	FlatList,
	ScrollView,
	Text,
	View,
	TouchableOpacity,
	Image,
	Dimensions,
	TouchableHighlight,
	Linking,
} from 'react-native';
import styles from './styles';
import Carousel, { Pagination } from 'react-native-snap-carousel';
// import { getIngredientName, getCategoryName, getCategoryById } from '../../data/MockDataAPI2';
import BackButton from '../../components/BackButton/BackButton';
import ViewIngredientsButton from '../../components/ViewIngredientsButton/ViewIngredientsButton';
const { width: viewportWidth } = Dimensions.get('window');
import PickerModal from 'react-native-picker-modal-view';

import UrlServer from '../../UrlServer.js';
let URL_SQS = UrlServer.getUrl();
var SQLite = require('react-native-sqlite-storage');
const db = SQLite.openDatabase(UrlServer.getNamaDatabaseSQLite(), '1.0', '', 1);

export default class PengaduanScreen extends React.Component {
	
	static navigationOptions = ({ navigation }) => {
		return {
			headerTransparent: 'true',
			headerLeft: (
				<BackButton
					onPress={() => {
						navigation.goBack();
					}}
				/>
			)
		};
	};
	
	constructor(props) {
		super(props);
		this.state = {
			activeSlide: 0
		};
	}
		
	renderImage = ({ item }) => {
		return (
			<TouchableHighlight>
				<View style={styles.imageContainer}>
					<Image style={styles.image} source={{ uri: item }} />
				</View>
			</TouchableHighlight>
		);
	};
	
	onPressIngredient = item => {
		var name = getIngredientName(item);
		let ingredient = item;
		this.props.navigation.navigate('Ingredient', { ingredient, name });
	};
	
	buttonClickListener = () => {
		alert("Clicked On Button !!!");
	};
	
	render() {
		const { activeSlide } = this.state;
		const { navigation } = this.props;
		const item = navigation.getParam('item');
		const category = "1"; //getCategoryById(item.categoryId);
		const title = "1"; //getCategoryName(category.id);
		
		return (
			<ScrollView style={styles.container}>
				<View style={styles.carouselContainer}>
					<View style={styles.carousel}>
						<Carousel
							ref={c => {
								this.slider1Ref = c;
							}}
							data={[URL_SQS + '/backend/web/uploads/' + item.gambar]}
							renderItem={this.renderImage}
							sliderWidth={viewportWidth}
							itemWidth={viewportWidth}
							inactiveSlideScale={1}
							inactiveSlideOpacity={1}
							firstItem={0}
							loop={false}
							autoplay={false}
							autoplayDelay={500}
							autoplayInterval={3000}
							onSnapToItem={index => this.setState({ activeSlide: index })}
						/>
						<Pagination
							// dotsLength={item.photosArray.length}
							activeDotIndex={activeSlide}
							containerStyle={styles.paginationContainer}
							dotColor="rgba(255, 255, 255, 0.92)"
							dotStyle={styles.paginationDot}
							inactiveDotColor="white"
							inactiveDotOpacity={0.4}
							inactiveDotScale={0.6}
							carouselRef={this.slider1Ref}
							tappableDots={!!this.slider1Ref}
						/>
					</View>
				</View>
				<View style={styles.infoRecipeContainer}>
					<Text style={styles.infoRecipeName}>{item.nama_dinas}</Text>
					<View style={styles.infoContainer}>
						<TouchableHighlight
							onPress={() => navigation.navigate('RecipesList', { category, title })}
						>
							<Text style={styles.category}>{item.keterangan}</Text>
						</TouchableHighlight>
					</View>
					<View style={styles.infoContainer}>
						<ViewIngredientsButton
							onPress={ ()=>{ Linking.openURL("https://www.google.com/maps/place/" + item.kordinat_lat + "," + item.kordinat_long)}}
						/>
					</View>
					<View style={{marginTop : 20}}>
						<Text style={{textAlign : 'center'}}>Proses : {item.status_proses}</Text>
						<Text style={{textAlign : 'center'}}>{item.nama_yang_mengerjakan} {item.no_hp_yang_mengerjakan}</Text>
					</View>
				</View>
			</ScrollView>
		);
	}
}