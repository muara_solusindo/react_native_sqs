import React from 'react';
import {
  Text,
  View,
  Dimensions,
  ScrollView,
  Image
} from 'react-native';
import styles from './styles';
import BackButton from '../../components/BackButton/BackButton';
const { width: viewportWidth } = Dimensions.get('window');

export default class BantuanScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTransparent: 'true',
      headerLeft: (
        <BackButton
          onPress={() => {
            navigation.goBack();
          }}
        />
      )
    };
  };

  render() {

    return (
		<View style={styles.kotakLuar}>
			<View style = {styles.kotakInformasi}>
				<Text style = {styles.fontAtas}>Pemerintah Kota Pematangsiantar</Text>
				<Text style = {styles.fontAtas2}>Bidang Administrasi Pemerintahan Umum</Text>
			</View>
			<Image
				style = {styles.logoPemerintah}
				source = {require('../../../assets/icons/logo.png')}
			/>
			<View style = {styles.kotakInformasi2}>
					<Text>JL. Merdeka No. 6, Kelurahan Dwikora</Text>
					<Text>Kecamatan Siantar Barat, Kota Pematangsiantar </Text>
			</View>
			
			<Text style={styles.link}>www.pematangsiantarkota.go.id</Text>
		</View>
    );
  }
}
