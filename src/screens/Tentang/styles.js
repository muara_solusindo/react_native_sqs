import { StyleSheet } from 'react-native';
import { RecipeCard } from '../../AppStyles';

const styles = StyleSheet.create({
	container: RecipeCard.container,
	photo: RecipeCard.photo,
	photohome: RecipeCard.photohome,
	title: RecipeCard.title,
	category: RecipeCard.category,
	logoPemerintah : {
		width : 110,
		height : 137 ,
		marginTop : 60,
		marginBottom : 40,
		padding : 15,
	},
	fontAtas : {
		fontSize :21, 
		color : 'white',
		fontWeight : 'bold'
	},
	fontAtas2 : {
		fontSize : 15, 
		color : 'white',
	},
	kotakInformasi : {
		alignItems : 'center',
		borderRadius : 8,
	},
	kotakInformasi2 : {
		alignItems : 'center',
		borderRadius : 7,
		backgroundColor: 'white',
		padding : 15,
		shadowColor: "#000000",
		shadowOpacity: 0.9,
		shadowRadius: 6,
		elevation : 7
	},
	kotakLuar: {
		backgroundColor: '#1ba0e1',
		// backgroundColor: 'white',
		flex: 1,
		    // flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	},
	link : {
		color : '#94d9ef',
		paddingTop : 20,
		fontStyle: 'italic',
		textDecorationLine : 'underline'
	},
});

export default styles;
