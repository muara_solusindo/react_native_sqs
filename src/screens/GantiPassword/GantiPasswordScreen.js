import React , {Component}from 'react';
import {
  Text,
  View,
  Dimensions,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ToastAndroid,
} from 'react-native';
import styles from './styles';
import BackButton from '../../components/BackButton/BackButton';
const{width : WIDTH} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';
import Spinner from 'react-native-loading-spinner-overlay';

import UrlServer from '../../UrlServer.js';
let URL_SQS = UrlServer.getUrl();
var SQLite = require('react-native-sqlite-storage');
const db = SQLite.openDatabase(UrlServer.getNamaDatabaseSQLite(), '1.0', '', 1);

export default class GantiPasswordScreen extends React.Component {
	static navigationOptions = {
		header: null
	};
	
	constructor(){
		super()
		this.state ={
			showPass : true,
			press : false,
			username : '',
			password_lama : '',
			password_baru : '',
			ulangi_password : '',
			loading : false,
		}
		this._isMounted = true;
	}
	
	componentWillUnmount() {
		this._isMounted = false;
	}
	
	ubahPassword = () => {
		if(this.state.password_baru.length < 6)
		{	alert('Password tidak boleh kurang dari 6 digit!');
		}
		else if(this.state.password_baru !== this.state.ulangi_password)
		{	alert('Konfirmasi password tidak sesuai!');
		}
		else
		{	if(this._isMounted) {
				this.setState({ loading : true });
			}
			db.transaction(function (txn) {
				txn.executeSql('select * from user', [], (tx, results) => {
					if(results.rows.length > 0) {
						prosesUbahPassword(results.rows.item(0).username);
					}
				});
			});
			prosesUbahPassword = (Username) => {
				fetch( URL_SQS + '/frontend/web/index.php?r=site/ubahpassword', {
					method : 'POST',
					headers : {
						'Accept' : 'application/json',
						'Content-Type' : 'application/json',
					},
					body : JSON.stringify({
						username : Username,
						passsword_lama : this.state.password_lama,
						password_baru : this.state.password_baru,
					})
				})
				.then((response) => response.json())
				.then((response) => {
					if(this._isMounted) {
						this.setState({ loading : false });
					}
					if(response.status == 1)
					{	ToastAndroid.show(response.pesan, ToastAndroid.SHORT);
						const { navigate } = this.props.navigation;
						navigate('Profil');
					}
					else
					{	alert('Gagal mengubah password');
					}
				})
				.catch((error) => {
					if(this._isMounted) {
						this.setState({ loading : false });
					}
				});
			}
		}
	}

	showPass = () => {
		if(this.state.press == false){
			this.setState({ showPass : false, press : true})
		}else{
			this.setState({ showPass : true, press : false})
		}
	}
	onPressLupa = () => {
		this.props.navigation.navigate('LupaPassword');
	};
	
	render() {
		return (
			<View style={{flex : 1}}>
				<Spinner visible={this.state.loading} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
				<ScrollView>
					<View style={[styles.blue]}/>
					<View style={styles.contents}>
						<View style={styles.profilLuarImage}>
							<View style={styles.shadowprofilImage}>
								<Image style={styles.profilImage} source={require('../../../assets/icons/sqs-bulet.png')}/>
							</View>
							<Text style={styles.profilAkun}> Siantar Quick Service</Text>
						</View>
						<View style={styles.profilCard}>
							<View style={{justifyContent: 'center', alignItems: 'center', marginVertical : 20}}>
								<View>
									<Text style={{fontSize : 15, fontStyle : 'italic', marginBottom : 5}}>Ganti Password</Text>
								</View>
								<View style={styles.inputContainer}>
									<Icon name={'ios-lock'} size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
									<TextInput 
										style={styles.input}
										placeholder={'Password Lama'}
										placeholderTextColor={'rgba(255,255,0.7)'}
										underlineColorAndroid = 'transparent'
										onChangeText={(password_lama) => this.setState({password_lama})}
									/>
								</View>
								<View style={styles.inputContainer}>
									<Icon name={'ios-lock'} size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
									<TextInput 
										style={styles.input}
										placeholder={'Password Baru'}
										placeholderTextColor={'rgba(255,255,0.7)'}
										underlineColorAndroid = 'transparent'
										onChangeText={(password_baru) => this.setState({password_baru})}
									/>
								</View>
								<View style={styles.inputContainer}>
									<Icon name={'ios-lock'} size={28} colot={'rgba(27,160,255,0.7)'} style={styles.inputIcon}/>
									<TextInput 
										style={styles.input}
										placeholder={'Ulangi Password'}
										placeholderTextColor={'rgba(255,255,0.7)'}
										underlineColorAndroid = 'transparent'
										onChangeText={(ulangi_password) => this.setState({ulangi_password})}
									/>
								</View>
								<View>
									<TouchableOpacity
										style={styles.luarBtnLogout}
										onPress = { this.ubahPassword }
									>
										<View style={styles.shadowBtnLogout}> 
											<View style={styles.btnLogout}> 
												<Text style={styles.btnName}>Ubah Password</Text>
											</View>
										</View>
									</TouchableOpacity>
								</View>
							</View>
						</View>
					</View>
				</ScrollView>
			</View>
		);
	}
}
