import { StyleSheet } from 'react-native';
import { RecipeCard } from '../../AppStyles';

const styles = StyleSheet.create({
	// container: RecipeCard.container,
	// photo: RecipeCard.photo,
	// photohome: RecipeCard.photohome,
	// title: RecipeCard.title,
	// category: RecipeCard.category,
	backgroundContainer: {
		flex: 1,
		width : null,
		height : null,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor : '#1ba0e1',
	},
	logo : {
		width : 120,
		height : 150
	},
	logoContainer : {
		alignItems: 'center',
	},
	logoText : {
		color : 'white',
		fontSize : 20,
		fontWeight: '500',
		marginTop : 10,
		opacity : 0.5
	},
	input : {
		width :  WIDTH - 55,
		height : 45,
		borderRadius: 25,
		fontSize : 16,
		paddingLeft : 45,
		backgroundColor : 'rgba(0,0,0,0.35)',
		color : 'rgba(255,255,255,0.7)',
		marginHorizontal: 25,
		marginVertical: 5 ,
	},
	inputIcon : { 
		position : 'absolute',
		top : 12,
		left : 37 
	},
	inputContainer : {
		marginTop : 10, 
	},
	btnEye : {
		position : 'absolute',
		top : 12,
		right : 37 
	},
	btnLogin : {
		width :  WIDTH - 55,
		height : 45,
		borderRadius: 25,
		backgroundColor : '#432577',
		justifyContent : 'center',
		marginTop : 20,
	},
	text : {
		color : 'rgba(255,255,255,0.7)',
		fontSize : 16,
		textAlign : 'center'
	}
});

export default styles;
