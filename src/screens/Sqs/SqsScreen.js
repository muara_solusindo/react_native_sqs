import React, {Component} from 'react';
import {
	Platform, 
	StyleSheet, 
	Text, 
	View,
	ImageBackground,
	Image,
	TextInput,
	Dimensions,
	TouchableOpacity
} from 'react-native';

import bgImage from '../../../assets/lainnya/union.jpeg';
import logo from '../../../assets/lainnya/logo.png';
import Icon from 'react-native-vector-icons/Ionicons';

const{width : WIDTH} = Dimensions.get('window');
// import styles from './styles';

import UrlServer from '../../UrlServer.js';
let URL_SQS = UrlServer.getUrl();
var SQLite = require('react-native-sqlite-storage');
const db = SQLite.openDatabase(UrlServer.getNamaDatabaseSQLite(), '1.0', '', 1);

export default class SqsScreen extends React.Component {
	static navigationOptions = {
		header: null
		};
		
	constructor(){
		super()
		this.state ={
			showPass : true,
			press : false,
			showPass : true,
			press : false,
			loading : false,
			userlogin : '',
		}
		this._isMounted = true;
	}
	
	componentDidMount() {
		const { navigation } = this.props;
		this.focusListener = navigation.addListener('didFocus', () => {
			db.transaction(function (txn) {
				txn.executeSql('select * from user', [], (tx, results) => {
					if(results.rows.length > 0) {
						goHome();
					}
				});
			});
			goHome = () => {
				const { navigate } = this.props.navigation;
				navigate('Home');
			}
		});
	}
	
	componentWillUnmount() {
		this._isMounted = false;
	}

	showPass = () => {
		if(this.state.press == false){
			this.setState({ showPass : false, press : true})
		}else{
			this.setState({ showPass : true, press : false})
		}
	}
	
	render() {
		const { navigation } = this.props;
		return (
			<ImageBackground source={bgImage} style={styles.backgroundContainer}>
				<View style={{
						backgroundColor : '#1ba0e1', 
						flex: 1,   
						width: null,  
						height: null, 
						position: 'absolute',
						top: 0,
						left: 0,
						right: 0,
						bottom: 0,
						opacity: 0.85,
						height : null,
						justifyContent: 'center',
						alignItems: 'center',
						}}>
					<View style={styles.logoContainer}>
						<TouchableOpacity onPress={() => {navigation.navigate('Home')}}>
							<Image source={logo} style={styles.logo}/>
						</TouchableOpacity>
						<Image style={{height: 45, width: 90 }} source={require('../../../assets/icons/aplikasi.png')}/>
						<Text style={styles.logoText}> 	( Siantar Quick Service )</Text>
						<Text style={styles.logoText2}> PEMERINTAH KOTA PEMATANGSIANTAR</Text>
					</View>
					<View style={{flexDirection: 'row'}}>
						<TouchableOpacity style={styles.btnSignIn} onPress={() => {navigation.navigate('Login')}}>
							<Text style={styles.textSignIn}>Masuk</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.btnSignUp} onPress={() => {navigation.navigate('Daftar')}} >
							<Text style={styles.textSignUp}>Daftar</Text>
						</TouchableOpacity>
					</View>
					
				</View>
		   </ImageBackground>
		);
	}
}

const styles = StyleSheet.create({
	backgroundContainer: {
		flex: 1,
		width : null,
		height : null,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor : '#1ba0e1',
	},
	logo : {
		width : 116,
		height : 146,
		marginBottom : 155,
	},
	logoContainer : {
		alignItems: 'center',
		marginBottom : 50,
	},
	logoText : {
		color : 'white',
		fontSize : 18,
		fontWeight: 'bold',
		marginTop : 10,
		opacity : 0.95,
		fontStyle: 'italic'
	},
	logoText2 : {
		color : 'white',
		fontSize : 18,
		fontWeight: 'bold',
		marginTop : 5,
		opacity : 1
	},
	input : {
		width :  WIDTH - 55,
		height : 45,
		borderRadius: 25,
		fontSize : 16,
		paddingLeft : 45,
		backgroundColor : 'rgba(0,0,0,0.35)',
		color : 'rgba(255,255,255,0.7)',
		marginHorizontal: 25,
		marginVertical: 5 ,
	},
	inputIcon : { 
		position : 'absolute',
		top : 12,
		left : 37 
	},
	inputContainer : {
		marginTop : 10, 
	},
	btnEye : {
		position : 'absolute',
		top : 12,
		right : 37 
	},
	btnLogin : {
		width :  WIDTH - 55,
		height : 50,
		borderRadius: 25,
		backgroundColor : '#432577',
		justifyContent : 'center',
		marginTop : 20,
	},
	btnSignIn : {
		// width :  WIDTH - 20,
		margin : 5,
		width : 160,
		height : 45,
		borderRadius: 25,
		backgroundColor : '#1ba0e1',
		justifyContent : 'center',
		marginTop : 20,
		borderWidth: 1.8,
		borderColor: 'white'
		
	},
	btnSignUp : {
		// width :  WIDTH - 55,
		margin : 5,
		width : 160,
		height : 45,
		borderRadius: 25,
		backgroundColor : 'white',
		justifyContent : 'center',
		marginTop : 20,
		borderWidth: 1.8,
		borderColor: '#1ba0e1'
	},
	text : {
		color : 'rgba(255,255,255,0.9)',
		fontSize : 20,
		textAlign : 'center'
	},
	textSignIn : {
		color : 'rgba(255,255,255,0.9)',
		fontSize : 17,
		textAlign : 'center',
		fontWeight : '400'
	},
	textSignUp : {
		color : '#1ba0e1',
		fontSize : 17,
		textAlign : 'center',
		fontWeight : '400'
	}
});
