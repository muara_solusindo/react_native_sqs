import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from '../screens/Home/HomeScreen';
import CategoriesScreen from '../screens/Categories/CategoriesScreen';
import PengurusanScreen from '../screens/Pengurusan/PengurusanScreen';
import BantuanScreen from '../screens/Bantuan/BantuanScreen';
import InputPengaduanScreen from '../screens/InputPengaduan/InputPengaduanScreen';
import AmbilLokasiScreen from '../screens/InputPengaduan/AmbilLokasiScreen';
import ProfilScreen from '../screens/Profil/ProfilScreen';
import PengaduanScreen from '../screens/Pengaduan/PengaduanScreen';
import LihatLokasiScreen from '../screens/LihatLokasi/LihatLokasiScreen';
import TentangScreen from '../screens/Tentang/TentangScreen';
import DrawerContainer from '../screens/DrawerContainer/DrawerContainer';
import SearchScreen from '../screens/Search/SearchScreen';
import SqsScreen from '../screens/Sqs/SqsScreen';
import LoginScreen from '../screens/Login/LoginScreen';
import LupaPasswordScreen from '../screens/LupaPassword/LupaPasswordScreen';
import DaftarScreen from '../screens/Daftar/DaftarScreen';
import GantiPasswordScreen from '../screens/GantiPassword/GantiPasswordScreen';
import GantiProfilScreen from '../screens/GantiProfil/GantiProfilScreen';

import UrlServer from '../UrlServer.js';
var SQLite = require('react-native-sqlite-storage');
const db = SQLite.openDatabase(UrlServer.getNamaDatabaseSQLite(), '1.0', '', 1);
db.transaction(function (txn) {
	txn.executeSql('create table if not exists user(nama varchar(100), username varchar(100), no_hp varchar(100))', []);
	txn.executeSql('create table if not exists log_pengaduan(id_log integer primary key, id_pengaduan integer, id_user integer, username varchar(100), id_lokasi integer, id_dinas integer, id_jenis_pengaduan integer, kordinat_lat varchar(50), kordinat_long varchar(50), keterangan varchar(255), gambar varchar(255), status_proses char(1), tanggal_proses datetime, created_at datetime, id_role integer, tanggal_koordinasi datetime, tanggal_teruskan datetime, tanggal_selesai datetime, tanggal_tolak datetime, nama_yang_mengerjakan varchar(100), no_hp_yang_mengerjakan varchar(100), nama_dinas varchar(100))', []);
	txn.executeSql('create table if not exists setting_mobile(nomor_hotline varchar(100))', []);
	// txn.executeSql('delete from log_pengaduan', []);
});

const MainNavigator = createStackNavigator(
  {
		Home: HomeScreen,
		Categories: CategoriesScreen,
		Pengurusan: PengurusanScreen,
		Bantuan: BantuanScreen,
		InputPengaduan : InputPengaduanScreen,
		AmbilLokasi : AmbilLokasiScreen,
		Pengaduan: PengaduanScreen,
		Profil: ProfilScreen,
		LihatLokasi: LihatLokasiScreen,
		Tentang: TentangScreen,
		Search: SearchScreen,
		Sqs: SqsScreen,
		Login: LoginScreen,
		LupaPassword: LupaPasswordScreen,
		Daftar: DaftarScreen,
		GantiPassword: GantiPasswordScreen,
		GantiProfil: GantiProfilScreen,
  },
  {
    initialRouteName: 'Sqs',
    defaulfNavigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        fontWeight: 'normal',
        textAlign: 'left',
        alignSelf: 'left',
        flex: 1,
        fontFamily: 'FallingSkyCond'
      }
    })
  }
);

const DrawerStack = createDrawerNavigator(
  {
    Main: MainNavigator
  },
  {
    drawerPosition: 'left',
    initialRouteName: 'Main',
    drawerWidth: 250,
    contentComponent: DrawerContainer
  }
);

export default AppContainer = createAppContainer(DrawerStack);

console.disableYellowBox = true;