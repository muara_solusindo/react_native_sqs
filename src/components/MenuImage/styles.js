import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  headerButtonContainer: {
	padding: 10,
  },
  headerButtonImage: {
    justifyContent: 'center',
    width: 17,
    height: 17,
    margin: 6
  }
});

export default styles;
