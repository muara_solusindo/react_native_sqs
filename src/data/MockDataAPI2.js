import { Text } from 'react-native';
import React, { Component } from 'react';
import { pengaduanterbarus, pengaduans, categories } from './dataPengaduan';

export function getCategoryById(categoryId) {
  let category;
  categories.map(data => {
    if (data.id == categoryId) {
      category = data;
    }
  });
  return category;
}


export function getCategoryName(categoryId) {
  let name;
  categories.map(data => {
    if (data.id == categoryId) {
      name = data.name;
    }
  });
  return name;
}

export function getPengaduans(categoryId) {
  const pengaduansArray = [];
  pengaduans.map(data => {
    if (data.categoryId == categoryId) {
		pengaduansArray.push(data);
    }
  });
  return pengaduansArray;
}
// Pengaduan Terbaru 
export function getPengaduanterbarus(categoryId) {
	const pengaduanterbarusArray = [];
	pengaduanterbarus.map(data => {
	  if (data.categoryId == categoryId) {
		  pengaduanterbarusArray.push(data);
	  }
	});
	return pengaduanterbarusArray;
  }
//

// modifica


export function getNumberOfPengaduans(categoryId) {
  let count = 0;
  pengaduans.map(data => {
    if (data.categoryId == categoryId) {
      count++;
    }
  });
  return count;
}

// functions for search

export function getPengaduansByCategoryName(categoryName) {
  const nameUpper = categoryName.toUpperCase();
  const pengaduansArray = [];
  categories.map(data => {
    if (data.name.toUpperCase().includes(nameUpper)) {
      const pengaduans = getPengaduans(data.id); // return a vector of recipes
      pengaduans.map(item => {
        pengaduansArray.push(item);
      });
    }
  });
  return pengaduansArray;
}

export function getPengaduansByPengaduanName(pengaduanName) {
  const nameUpper = pengaduanName.toUpperCase();
  const pengaduansArray = [];
  pengaduans.map(data => {
    if (data.title.toUpperCase().includes(nameUpper)) {
      pengaduansArray.push(data);
    }
  });
  return pengaduansArray;
}
