export const categories = [
  
  {
    id: 1,
    name: 'Kategori 1',
    photo_url: 'https://ak1.picdn.net/shutterstock/videos/19498861/thumb/1.jpg'
  },
  {
    id: 2,
    name: 'Kategori 2',
    photo_url:
      'https://images.unsplash.com/photo-1533777324565-a040eb52facd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
  },
  {
    id: 3,
    name: 'Kategori 3',
    photo_url:
    'https://www.telegraph.co.uk/content/dam/Travel/2019/January/france-food.jpg?imwidth=1400'
  },
];

export const pengaduans = [
  {
    pengaduanId: 1,
    categoryId: 3,
    title: 'Pengaduan 1',
    photo_source : '../../../assets/lainnya/contoh-gambar.png',
    photo_url: 'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
	photosArray: [
		'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
		"https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png",
	],
    description:
	  'Detail Keterangan Pengaduan Disini ... ',
	},

  {
    pengaduanId: 2,
    categoryId: 3,
    title: 'Pengaduan 2',
	photo_source : '../../../assets/lainnya/contoh-gambar.png',
    photo_url: 'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
	photosArray: [
		'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
		"https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png",
	],
	description:
		'Detail Keterangan Pengaduan Disini ... ',
  	},
  {
    pengaduanId: 3,
    categoryId: 2,
    title: 'Pengaduan 3',
	photo_source : '../../../assets/lainnya/contoh-gambar.png',
    photo_url: 'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
	photosArray: [
		'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
		"https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png",
	],
	description:
		'Detail Keterangan Pengaduan Disini ... ',
	},
  {
    pengaduanId: 4,
    categoryId: 2,
    title: 'Pengaduan 4',
	photo_source : '../../../assets/lainnya/contoh-gambar.png',
    photo_url: 'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
	photosArray: [
		'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
		"https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png",
	],
	description:
		'Detail Keterangan Pengaduan Disini ... ',
	},
  {
    pengaduanId: 5,
    categoryId: 1,
    title: 'Pengaduan 5',
	photo_source : '../../../assets/lainnya/contoh-gambar.png',
    photo_url: 'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
	photosArray: [
		'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
		"https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png",
	],
	description:
		'Detail Keterangan Pengaduan Disini ... ',
	},
  {
    pengaduanId: 6,
    categoryId: 1,
    title: 'Pengaduan 5',
	photo_source : '../../../assets/lainnya/contoh-gambar.png',
    photo_url: 'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
	photosArray: [
		'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
		"https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png",
	],
	description:
		'Detail Keterangan Pengaduan Disini ... ',
	},
];

export const pengaduanterbarus = [
  {
    pengaduanId: 1,
    categoryId: 3,
    title: 'Pengaduan 1',
    photo_source : '../../../assets/lainnya/contoh-gambar.png',
    photo_url: 'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
	photosArray: [
		'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
		"https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png",
	],
    description:
	  'Detail Keterangan Pengaduan Disini ... ',
	},

  {
    pengaduanId: 2,
    categoryId: 3,
    title: 'Pengaduan 2',
	photo_source : '../../../assets/lainnya/contoh-gambar.png',
    photo_url: 'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
	photosArray: [
		'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
		"https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png",
	],
	description:
		'Detail Keterangan Pengaduan Disini ... ',
  	},
  {
    pengaduanId: 3,
    categoryId: 2,
    title: 'Pengaduan 3',
	photo_source : '../../../assets/lainnya/contoh-gambar.png',
    photo_url: 'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
	photosArray: [
		'https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png',
		"https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png",
	],
	description:
		'Detail Keterangan Pengaduan Disini ... ',
	}
];

